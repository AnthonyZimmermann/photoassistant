#!/bin/bash

# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

# This script relies on python3, pip3, git and entr being installed and is intended for development purpose only.
python3 -m venv pytest-env
source pytest-env/bin/activate

pip3 install --upgrade pip
pip3 install pytest

pip3 install -e .
pip3 freeze

sleep 1
pytest_args="-v --full-trace"
pytest_args="-v -x"
pytest_args="-v"

while true; do
    { git ls-files . --exclude-standard --others; } | entr -d -s "pytest ${pytest_args}"
    sleep 0.2
done
