# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import pytest
import logging

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

def test_loading():
    import photoassistant
