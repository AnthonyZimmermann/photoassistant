# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import json
import datetime
import subprocess
import sys

if __name__ == "__main__":
    filenames = sys.argv[1:]
    for filename in filenames:
        with open(filename, "r") as file_:
            data = json.loads(file_.read())
        if "data" not in data:
            continue
        data = data["data"]
        for hash, subdata in data.items():
            if "update_timestamp" in subdata:
                update_timestamp = subdata["update_timestamp"]
                try:
                    new_update_timestamp = int(datetime.datetime.fromisoformat(update_timestamp).timestamp() * 1000)
                    subprocess.run(f"sed -i s/{update_timestamp}/{new_update_timestamp}/ {filename}".split())
                except:
                    pass
            if "meta" not in subdata:
                continue
            meta = subdata["meta"]
            if "creation_timestamp" in meta:
                creation_timestamp = meta["creation_timestamp"]
                try:
                    new_creation_timestamp = int(datetime.datetime.fromisoformat(creation_timestamp).timestamp() * 1000)
                    subprocess.run(f"sed -i s/{creation_timestamp}/{new_creation_timestamp}/ {filename}".split())
                except:
                    pass
