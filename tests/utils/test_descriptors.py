# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import logging
import pytest
import re

from photoassistant.utils.descriptors import RDescriptor
from photoassistant.utils.descriptors import RWDescriptor
from photoassistant.utils.descriptors import SettingsDescriptor

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

class TestRDescriptor:
    DESCRIPTOR_CLASS = RDescriptor

    def test_instantiation(self):
        class Test:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
        Test()

    def test_instantiation_with_get_method(self):
        class Test:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
            def get_var(self): pass
        Test()

    def test_instantiation_with_inheritance_and_get_method(self):
        class TestBase:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
        class Test(TestBase):
            def get_var(self): pass
        Test()

    def test_instantiation_with_inheritance_and_overwritten_get_method(self):
        class TestBase:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
            def get_var(self): pass
        class Test(TestBase):
            def get_var(self): pass
        Test()

    def test_throw_now_implemented_error_with_undefined_get_method(self):
        class Test:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
        t = Test()
        with pytest.raises(NotImplementedError):
            assert t.var == None

    def test_get_method(self):
        class Test:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
            def get_var(self): return 17
        t = Test()
        assert t.get_var() == 17
        assert t.var == 17

    def test_get_method_with_inheritance(self):
        class TestBase:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
        class Test(TestBase):
            def get_var(self): return 17
        t = Test()
        assert t.get_var() == 17
        assert t.var == 17

    def test_overwritten_get_method_with_inheritance(self):
        class TestBase:
            var = TestRDescriptor.DESCRIPTOR_CLASS()
            def get_var(self): return 17
        class Test(TestBase):
            def get_var(self): return 19
        t = Test()
        assert t.get_var() == 19
        assert t.var == 19


class TestRWDescriptor(TestRDescriptor):
    DESCRIPTOR_CLASS = RWDescriptor

    def test_instantiation_with_set_method(self):
        class Test:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def set_var(self, value): pass
        Test()

    def test_instantiation_with_get_method_and_set_method(self):
        class Test:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def get_var(self): pass
            def set_var(self, value): pass
        Test()

    def test_instantiation_with_inheritance_and_set_method(self):
        class TestBase:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
        class Test(TestBase):
            def set_var(self, value): pass
        Test()

    def test_instantiation_with_inheritance_and_get_method_and_set_method(self):
        class TestBase:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
        class Test(TestBase):
            def set_var(self, value): pass
            def get_var(self): pass
        Test()

    def test_instantiation_with_inheritance_and_overwritten_set_method(self):
        class TestBase:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def set_var(self, value): pass
        class Test(TestBase):
            def set_var(self, value): pass
        Test()

    def test_instantiation_with_inheritance_and_overwritten_get_method_and_set_method(self):
        class TestBase:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def set_var(self, value): pass
            def get_var(self): pass
        class Test(TestBase):
            def set_var(self, value): pass
            def get_var(self): pass
        Test()

    def test_throw_now_implemented_error_with_undefined_set_method(self):
        class Test:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
        t = Test()
        with pytest.raises(NotImplementedError):
            t.var = 5

    def test_throw_now_implemented_error_with_undefined_get_method_and_undefined_set_method(self):
        class Test:
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
        t = Test()
        with pytest.raises(NotImplementedError):
            assert t.var == None
        with pytest.raises(NotImplementedError):
            t.var = 5

    def test_set_method(self):
        class Test:
            _var = 0
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def set_var(self, value):
                self._var = value
        t = Test()
        assert t._var == 0
        t.set_var(19)
        assert t._var == 19
        t.var = 23
        assert t._var == 23

    def test_get_method_and_set_method(self):
        class Test:
            _var = 7
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def get_var(self):
                return self._var
            def set_var(self, value):
                self._var = value
        t = Test()
        assert t.get_var() == 7
        assert t.var == 7
        t.set_var(11)
        assert t.get_var() == 11
        assert t.var == 11
        t.var = 13
        assert t.get_var() == 13
        assert t.var == 13

    def test_set_method_with_inheritance(self):
        class TestBase:
            _var = 0
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
        class Test(TestBase):
            def set_var(self, value):
                self._var = value
        t = Test()
        assert t._var == 0
        t.set_var(19)
        assert t._var == 19
        t.var = 23
        assert t._var == 23

    def test_get_method_and_set_method_with_inheritance(self):
        class TestBase:
            _var = 7
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
        class Test(TestBase):
            def get_var(self):
                return self._var
            def set_var(self, value):
                self._var = value
        t = Test()
        assert t.get_var() == 7
        assert t.var == 7
        t.set_var(11)
        assert t.get_var() == 11
        assert t.var == 11
        t.var = 13
        assert t.get_var() == 13
        assert t.var == 13

    def test_overwritten_set_method_with_inheritance(self):
        class TestBase:
            _var = 7
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def set_var(self, value):
                self._var = value
        class Test(TestBase):
            def set_var(self, value):
                super().set_var(value)
                self._var += 1
        t = Test()
        assert t._var == 7
        t.set_var(19)
        assert t._var == 20
        t.var = 23
        assert t._var == 24

    def test_overwritten_get_method_and_overwritten_set_method_with_inheritance(self):
        class TestBase:
            _var = 7
            var = TestRWDescriptor.DESCRIPTOR_CLASS()
            def get_var(self):
                return self._var
            def set_var(self, value):
                self._var = value
        class Test(TestBase):
            def get_var(self):
                return -self._var
            def set_var(self, value):
                super().set_var(value)
                self._var += 1
        t = Test()
        assert t._var == 7
        assert t.var == -7
        assert t.get_var() == -7
        t.set_var(19)
        assert t._var == 20
        assert t.var == -20
        assert t.get_var() == -20
        t.var = 23
        assert t._var == 24
        assert t.var == -24
        assert t.get_var() == -24


class TestSettingsDescriptor:
    def test_instantiation(self):
        class Test:
            test_string = SettingsDescriptor("hello world")
            test_int = SettingsDescriptor(17, int)
            test_float = SettingsDescriptor(17.2, float)
            test_bytes = SettingsDescriptor(b"\x01\x02\x03", bytes.fromhex, lambda value: value.hex())
        Test()

    def test_instation_fails_on_bad_default_values(self):
        with pytest.raises((AttributeError, RuntimeError)):
            class Test:
                test_int = SettingsDescriptor("hello", int)
            Test()

    def test_read(self):
        class Test:
            test_setting = SettingsDescriptor("hello world")
        t = Test()
        assert t.test_setting == "hello world"

    def test_set_and_read_string(self):
        class Test:
            test_setting = SettingsDescriptor("hello world")
        t = Test()
        assert t.test_setting == "hello world"
        t.test_setting = "hello"
        assert t.test_setting == "hello"

    def test_set_using_setattr(self):
        class Test:
            test_setting = SettingsDescriptor("hello world")
        t = Test()
        setattr(t, "test_setting", "hello back")
        assert t.test_setting == "hello back"

    def test_set_fails_on_incompatible_input(self):
        class Test:
            # allow only single words without e.g., white spaces
            test_setting = SettingsDescriptor("hello", lambda value: re.search(r"(\w*)", value).group(1))
        t = Test()
        with pytest.raises(ValueError):
            t.test_setting = "hello world"

    def test_set_and_read_int(self):
        class Test:
            test_setting = SettingsDescriptor(14, int)
        t = Test()
        assert t.test_setting == 14
        t.test_setting = 12
        assert t.test_setting == 12
        t.test_setting = "9"
        assert t.test_setting == 9

    def test_set_and_read_bytes(self):
        class Test:
            test_setting = SettingsDescriptor(b"\x01\x02\x03", bytes.fromhex, lambda value: value.hex())
        t = Test()
        assert t.test_setting == b"\x01\x02\x03"
        t.test_setting = "hello world".encode()
        assert t.test_setting == "hello world".encode()

    def test_settings_are_stored_in_settings_attribute(self):
        class Test:
            test_string = SettingsDescriptor("hello world")
            test_int = SettingsDescriptor(17, int)
            test_float = SettingsDescriptor(17.2, float)
            test_bytes = SettingsDescriptor(b"\x01\x02\x03", bytes.fromhex, lambda value: value.hex())
        t = Test()
        expected_settings = dict(
            test_string="hello world",
            test_int='17',
            test_float='17.2',
            test_bytes="010203",
        )
        assert t._settings == expected_settings

    def test_set_and_read_in_multiple_instances(self):
        class Test:
            test_setting = SettingsDescriptor("hello world")
        t1 = Test()
        t2 = Test()
        assert t1.test_setting == "hello world"
        assert t2.test_setting == "hello world"
        t1.test_setting = "hello"
        assert t1.test_setting == "hello"
        assert t2.test_setting == "hello world"
        t2.test_setting = "hola"
        assert t1.test_setting == "hello"
        assert t2.test_setting == "hola"
