# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import logging
import multiprocessing
import multiprocessing.pool
import queue
import re
import time
import threading

from photoassistant.utils.cachingutils import CacheObject
from photoassistant.utils.cachingutils import CacheState
from photoassistant.utils.cachingutils import SizeRestrictedCache
from photoassistant.utils.cachingutils import AsyncSharedCache
from photoassistant.utils.cachingutils import AsyncSharedCacheQueryPolicy

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)


class TestAsyncSharedCacheQueryPolicy:
    def test_async_shared_cache_query_policy_instantiation(self):
        AsyncSharedCacheQueryPolicy(self, lambda: None)
        AsyncSharedCacheQueryPolicy(self, lambda: None, lambda _: None)

    def test_async_shared_cache_query_policy_sets_query_owner_correctly(self):
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, lambda: None)
        assert async_shared_cache_query_policy.query_owner == self

    def test_async_shared_cache_query_policy_can_set_result_placeholder_None(self):
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, lambda: None)
        async_shared_cache_query_policy.set_result_placeholder(None)

    def test_async_shared_cache_query_policy_can_set_result_placeholder_to_arbitrary_value(self):
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, lambda: None)
        async_shared_cache_query_policy.set_result_placeholder("abc")
        async_shared_cache_query_policy.set_result_placeholder(0)
        async_shared_cache_query_policy.set_result_placeholder(17.0)
        async_shared_cache_query_policy.set_result_placeholder((None, None))
        async_shared_cache_query_policy.set_result_placeholder([None, None])
        async_shared_cache_query_policy.set_result_placeholder(dict(a=1, b=2, c=3))
        async_shared_cache_query_policy.set_result_placeholder(object())
        async_shared_cache_query_policy.set_result_placeholder(AsyncSharedCacheQueryPolicy)

    def test_async_shared_cache_query_policy_can_delete_result_placeholder(self):
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, lambda: None)
        async_shared_cache_query_policy.delete_result_placeholder()

    def test_async_shared_cache_query_policy_get_result_placeholder_returns_special_NONE_object_if_no_placeholder_set(self):
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, lambda: None)
        set_delete_order = ["set", "set", "del", "set", "del", "set", "del", "del", "del", "set", "set", "set", "del"]

        assert async_shared_cache_query_policy.result_placeholder == AsyncSharedCacheQueryPolicy.NONE
        for action in set_delete_order:
            if action == "set":
                async_shared_cache_query_policy.set_result_placeholder("dummy")
                assert async_shared_cache_query_policy.result_placeholder != AsyncSharedCacheQueryPolicy.NONE
            else: # action == "del"
                async_shared_cache_query_policy.delete_result_placeholder()
                assert async_shared_cache_query_policy.result_placeholder == AsyncSharedCacheQueryPolicy.NONE


class TestCacheObject:
    def test_cache_object_can_be_created(self):
        CacheObject()

    def test_cache_object_data_can_be_set_and_retrieved(self):
        cache_object = CacheObject()
        assert cache_object.data == None
        cache_object.data = "hello world"
        assert cache_object.data == "hello world"

    def test_cache_object_touched_returns_false_before_touching(self):
        cache_object = CacheObject()
        assert cache_object.touched == False

    def test_cache_object_touched_returns_true_after_touching(self):
        cache_object = CacheObject()
        cache_object.touch()
        assert cache_object.touched == True

    def test_cache_object_last_access_is_creation_time_before_touching(self):
        t_before = time.time()
        cache_object = CacheObject()
        t_after = time.time()
        assert cache_object.last_access > t_before
        assert cache_object.last_access < t_after

    def test_cache_object_last_access_is_updated_with_touching(self):
        cache_object = CacheObject()
        for _ in range(3):
            t_last = cache_object.last_access
            t_before = time.time()
            cache_object.touch()
            t_after = time.time()
            assert cache_object.last_access > t_last
            assert cache_object.last_access >= t_before
            assert cache_object.last_access <= t_after


class TestSizeRestrictedCache:
    def test_size_restricted_cache_instantiation(self):
        SizeRestrictedCache(10)

    def test_size_restricted_cache_len_is_zero_after_instantiation(self):
        size_restricted_cache = SizeRestrictedCache(10)
        assert len(size_restricted_cache) == 0

    def test_get_cache_state(self):
        size_restricted_cache = SizeRestrictedCache(10)
        owner = object()
        owner_function = object()

        # no data in cache
        assert size_restricted_cache.get_cache_state(owner, owner_function, "cache_key_1") == CacheState.DATA_MISSING
        assert size_restricted_cache.get_cache_state(owner, owner_function, "cache_key_1") == CacheState.DATA_MISSING

        # cache has been queried once but no data set/ready
        size_restricted_cache.query_cache_object(owner, owner_function, "cache_key_1")
        assert size_restricted_cache.get_cache_state(owner, owner_function, "cache_key_1") == CacheState.DATA_WAITING
        assert size_restricted_cache.get_cache_state(owner, owner_function, "cache_key_1") == CacheState.DATA_WAITING

        # cache has been queried once and data is set/ready
        cache_object = size_restricted_cache.query_cache_object(owner, owner_function, "cache_key_1")
        cache_object.set_data(123)
        assert size_restricted_cache.get_cache_state(owner, owner_function, "cache_key_1") == CacheState.DATA_READY
        assert size_restricted_cache.get_cache_state(owner, owner_function, "cache_key_1") == CacheState.DATA_READY

    def test_size_restricted_cache_call_of_query_cache_object_creates_cache_object(self):
        size_restricted_cache = SizeRestrictedCache(10)
        owner = object()
        owner_function = object()
        cache_object = size_restricted_cache.query_cache_object(owner, owner_function, "cache_key_1")
        assert len(size_restricted_cache) == 1
        assert isinstance(cache_object, CacheObject)

    def test_size_restricted_cache_call_of_query_cache_object_returns_already_existing_cache_object_on_match(self):
        size_restricted_cache = SizeRestrictedCache(10)
        owner = object()
        owner_function = object()
        cache_object_1 = size_restricted_cache.query_cache_object(owner, owner_function, "cache_key_1")
        cache_object_2 = size_restricted_cache.query_cache_object(owner, owner_function, "cache_key_1")
        assert len(size_restricted_cache) == 1
        assert cache_object_1 == cache_object_2

    def test_size_restricted_cache_keeps_seperate_cache_for_different_owners(self):
        size_restricted_cache = SizeRestrictedCache(10)
        owner1 = object()
        owner1_function = object()
        owner2 = object()
        owner2_function = object()
        cache_object_1 = size_restricted_cache.query_cache_object(owner1, owner1_function, "cache_key_1")
        cache_object_2 = size_restricted_cache.query_cache_object(owner2, owner2_function, "cache_key_1")
        assert len(size_restricted_cache) == 2
        assert cache_object_1 != cache_object_2

    def test_size_restricted_cache_deletes_correct_cache_entry_if_no_slots_left(self):
        #    t0  |   t1  |   t2  |   t3  |   t4  |        |   t5  |   t6  |   t7 
        #   o1k2 |  o1k2 |  o1k2 |       | No1k2 | touch2 |  o1k2 |       | N2o1k2
        #        |  o2k1 |  o2k1 |  o2k1 |       |        | No2k1 |  o2k1 |  o2k1
        #        |       |  o1k1 |  o1k1 |  o1k1 | touch1 |       | No1k1 |  o1k1
        #        |       |       |  o1k3 |  o1k3 | touch3 |  o1k3 |  o1k3 |      
        size_restricted_cache = SizeRestrictedCache(3)
        owner1 = object()
        owner1_function = object()
        owner2 = object()
        owner2_function = object()
        cache_object_o1_k2 = size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_2")
        cache_object_o2_k1 = size_restricted_cache.query_cache_object(owner2, owner2_function, "o2_cache_key_1")
        cache_object_o1_k1 = size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_1")
        assert len(size_restricted_cache) == 3
        assert cache_object_o1_k2 != cache_object_o2_k1
        assert cache_object_o2_k1 != cache_object_o1_k1
        assert cache_object_o1_k2 != cache_object_o1_k1

        cache_object_o1_k3 = size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_3")

        size_restricted_cache.wait_for_delete_thread()
        assert len(size_restricted_cache) == 3
        assert cache_object_o1_k2 != cache_object_o2_k1
        assert cache_object_o1_k2 != cache_object_o1_k1
        assert cache_object_o1_k2 != cache_object_o1_k3
        assert cache_object_o2_k1 != cache_object_o1_k1
        assert cache_object_o2_k1 != cache_object_o1_k3
        assert cache_object_o1_k1 != cache_object_o1_k3

        assert cache_object_o1_k3 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_3")
        assert cache_object_o1_k1 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_1")
        assert cache_object_o2_k1 == size_restricted_cache.query_cache_object(owner2, owner2_function, "o2_cache_key_1")
        new_cache_object_o1_k2 = size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_2")
        assert new_cache_object_o1_k2 != cache_object_o1_k2

        size_restricted_cache.wait_for_delete_thread()
        assert len(size_restricted_cache) == 3

        cache_object_o1_k1.touch()
        new_cache_object_o1_k2.touch()
        cache_object_o1_k3.touch()
        cache_object_o2_k1.touch() # not cached anymore

        assert len(size_restricted_cache) == 3
        assert new_cache_object_o1_k2 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_2")
        assert cache_object_o1_k1 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_1")
        assert cache_object_o1_k3 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_3")

        new_cache_object_o2_k1 = size_restricted_cache.query_cache_object(owner2, owner2_function, "o2_cache_key_1")
        assert new_cache_object_o2_k1 != cache_object_o2_k1

        size_restricted_cache.wait_for_delete_thread()
        assert len(size_restricted_cache) == 3
        assert new_cache_object_o1_k2 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_2")
        assert cache_object_o1_k3 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_3")
        assert new_cache_object_o2_k1 == size_restricted_cache.query_cache_object(owner2, owner2_function, "o2_cache_key_1")

        new_cache_object_o1_k1 = size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_1")
        assert new_cache_object_o1_k1 != cache_object_o1_k1

        size_restricted_cache.wait_for_delete_thread()
        assert len(size_restricted_cache) == 3
        assert cache_object_o1_k3 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_3")
        assert new_cache_object_o2_k1 == size_restricted_cache.query_cache_object(owner2, owner2_function, "o2_cache_key_1")
        assert new_cache_object_o1_k1 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_1")

        new2_cache_object_o1_k2 = size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_2")
        assert new2_cache_object_o1_k2 != new_cache_object_o1_k2
        assert new2_cache_object_o1_k2 != cache_object_o1_k2

        size_restricted_cache.wait_for_delete_thread()
        assert len(size_restricted_cache) == 3
        assert new_cache_object_o2_k1 == size_restricted_cache.query_cache_object(owner2, owner2_function, "o2_cache_key_1")
        assert new_cache_object_o1_k1 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_1")
        assert new2_cache_object_o1_k2 == size_restricted_cache.query_cache_object(owner1, owner1_function, "o1_cache_key_2")

    def test_size_restricted_cache_delete_frees_slot(self):
        size_restricted_cache = SizeRestrictedCache(10)
        owner = object()
        owner_function = object()
        cache_object = size_restricted_cache.query_cache_object(owner, owner_function, "key_1")
        assert len(size_restricted_cache) == 1
        size_restricted_cache.delete(owner)
        assert len(size_restricted_cache) == 0
        assert cache_object != size_restricted_cache.query_cache_object(owner, owner_function, "key_1")

    def test_size_restricted_cache_delete_removes_only_all_owned_cache_objects_when_called_without_key(self):
        size_restricted_cache = SizeRestrictedCache(10)
        owner1 = object()
        owner1_function = object()
        owner2 = object()
        owner2_function = object()
        cache_object_1 = size_restricted_cache.query_cache_object(owner1, owner1_function, "key_1")
        cache_object_2 = size_restricted_cache.query_cache_object(owner1, owner1_function, "key_2")
        cache_object_3 = size_restricted_cache.query_cache_object(owner2, owner2_function, "dummy")
        assert len(size_restricted_cache) == 3
        size_restricted_cache.delete(owner1)
        assert len(size_restricted_cache) == 1
        assert cache_object_3 == size_restricted_cache.query_cache_object(owner2, owner2_function, "dummy")
        assert cache_object_1 != size_restricted_cache.query_cache_object(owner1, owner1_function, "key_1")
        assert cache_object_2 != size_restricted_cache.query_cache_object(owner1, owner1_function, "key_2")


class TestAsyncSharedCache:
    def test_async_shared_cache_can_be_created(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        AsyncSharedCache(slots=10, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)

    def test_async_shared_cache_can_be_created_with_default_workers(self):
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        AsyncSharedCache(slots=10, waiter_pool=threadpool_waiters)

    def test_async_shared_cache_can_be_created_with_default_waiters(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        AsyncSharedCache(slots=10, worker_pool=threadpool_workers)

    def test_async_shared_cache_can_be_created_with_default_workers_and_waiters(self):
        AsyncSharedCache(slots=10)

    def test_async_shared_cache_can_execute_without_result_placeholder(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        async_shared_cache = AsyncSharedCache(slots=10, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)

        def func(num):
            return f"{num}"

        def callback(_):
            pass

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func, callback)
        result = async_shared_cache.query(async_shared_cache_query_policy, 2)
        assert result == "2"

    def test_get_slots(self):
        async_shared_cache = AsyncSharedCache(slots=9)
        assert async_shared_cache.slots == 9

    def test_set_slots(self):
        async_shared_cache = AsyncSharedCache(slots=9)
        assert async_shared_cache.slots == 9
        async_shared_cache.slots = 3
        assert async_shared_cache.slots == 3

    def test_async_shared_cache_can_execute_without_result_placeholder_and_without_callback(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        async_shared_cache = AsyncSharedCache(slots=10, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)

        def func(num):
            return f"{num}"

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func)
        result = async_shared_cache.query(async_shared_cache_query_policy, 3)
        assert result == "3"

    def test_async_shared_cache_access_same_cache_object_for_equivalent_instances_of_async_shared_cache_query_policy(self):
        async_shared_cache = AsyncSharedCache(slots=10)

        def func(num):
            return f"{time.time()} {num}"

        result = async_shared_cache.query(AsyncSharedCacheQueryPolicy(self, func), 3)
        assert re.match(r"\d+(\.\d+)? 3", result) is not None
        assert result.split()[-1] == "3"
        assert async_shared_cache.query(AsyncSharedCacheQueryPolicy(self, func), 3) == result

    def test_async_shared_cache_can_execute_async_with_result_placeholder_with_default_pools(self):
        async_shared_cache = AsyncSharedCache(slots=10)
        unblock_event = threading.Event()
        ready_event = threading.Event()
        def func(num):
            unblock_event.wait()
            return f"{num}"
        def callback(_):
            ready_event.set()
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func, callback)
        async_shared_cache_query_policy.set_result_placeholder(None)
        result = async_shared_cache.query(async_shared_cache_query_policy, 5)
        assert result == None
        unblock_event.set()
        ready_event.wait()
        result = async_shared_cache.query(async_shared_cache_query_policy, 5)
        assert result == "5"

    def test_async_shared_cache_can_execute_async_with_result_placeholder(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        async_shared_cache = AsyncSharedCache(slots=10, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)

        async_results = queue.Queue()
        def func(num):
            return f"{num}"

        def callback(num_string):
            async_results.put(num_string)

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func, callback)
        async_shared_cache_query_policy.set_result_placeholder(None)
        result = async_shared_cache.query(async_shared_cache_query_policy, 5)
        assert result == None

        result = async_results.get(timeout=0.1)
        assert result == "5"

        async_shared_cache_query_policy.set_result_placeholder("abc")
        result = async_shared_cache.query(async_shared_cache_query_policy, 7)
        assert result == "abc"
        result = async_results.get(timeout=0.1)
        assert result == "7"

    def test_async_shared_cache_executes_callback_without_result_placeholder(self):
        async_shared_cache = AsyncSharedCache(slots=10)
        class LockedCounter:
            def __init__(self):
                self.lock = threading.Lock()
                self.count = 0
            def increment(self):
                with self.lock:
                    self.count += 1
                    return self.count

        counter_function_execution = LockedCounter()
        counter_callback_execution = LockedCounter()
        callback_executed_event = threading.Event()
        def func(value):
            counter_function_execution.increment()
            return f"-{value}-"
        def callback(_):
            counter_callback_execution.increment()
            callback_executed_event.set()
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func, callback)

        result = async_shared_cache.query(async_shared_cache_query_policy, 11)
        assert result == "-11-"
        callback_executed_event.wait()
        assert counter_function_execution.count == 1
        assert counter_callback_execution.count == 1

        callback_executed_event.clear()
        result = async_shared_cache.query(async_shared_cache_query_policy, 11)
        assert result == "-11-"
        callback_executed_event.wait()
        assert counter_function_execution.count == 1
        assert counter_callback_execution.count == 2

    def test_get_cache_state(self):
        async_shared_cache = AsyncSharedCache(slots=10)
        def func(num):
            time.sleep(0.001)
            return f"{num}"

        assert async_shared_cache.get_cache_state(AsyncSharedCacheQueryPolicy(self, func), 3) == CacheState.DATA_MISSING
        assert async_shared_cache.get_cache_state(AsyncSharedCacheQueryPolicy(self, func), 3) == CacheState.DATA_MISSING

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func)
        async_shared_cache_query_policy.set_result_placeholder(None)
        assert async_shared_cache.query(async_shared_cache_query_policy, 3) == None
        assert async_shared_cache.get_cache_state(AsyncSharedCacheQueryPolicy(self, func), 3) == CacheState.DATA_WAITING

        assert async_shared_cache.query(AsyncSharedCacheQueryPolicy(self, func), 3) == "3"
        assert async_shared_cache.get_cache_state(AsyncSharedCacheQueryPolicy(self, func), 3) == CacheState.DATA_READY

    def test_async_shared_cache_caches_result(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        async_shared_cache = AsyncSharedCache(slots=10, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)

        async_results = queue.Queue()
        def func():
            return time.time()

        def callback(t):
            async_results.put(t)

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func, callback)
        async_shared_cache_query_policy.set_result_placeholder(None)
        t_before = time.time()
        result = async_shared_cache.query(async_shared_cache_query_policy)
        assert result == None

        result = async_results.get(timeout=0.1)
        t_after = time.time()
        assert result > t_before
        assert result <= t_after
        t_last = result
        result = async_shared_cache.query(async_shared_cache_query_policy)
        assert result == t_last

    def test_async_shared_cache_caches_results_for_different_arguments_in_different_slots(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        async_shared_cache = AsyncSharedCache(slots=10, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)

        async_results = queue.Queue()
        def func(i):
            return time.time(), i

        def callback(t):
            async_results.put(t)

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func, callback)
        async_shared_cache_query_policy.set_result_placeholder(None)
        t_before = time.time()
        result_11 = async_shared_cache.query(async_shared_cache_query_policy, 11)
        assert result_11 == None
        result_11 = async_results.get(timeout=0.1)
        t_after = time.time()
        assert result_11[1] == 11
        assert result_11[0] > t_before
        assert result_11[0] <= t_after

        t_before = time.time()
        assert t_before > t_after
        result_13 = async_shared_cache.query(async_shared_cache_query_policy, 13)
        assert result_13 != result_11
        assert result_13 == None
        result_13 = async_results.get(timeout=0.1)
        t_after = time.time()
        assert result_13[1] == 13
        assert result_13[0] > t_before
        assert result_13[0] <= t_after

        result = async_shared_cache.query(async_shared_cache_query_policy, 11)
        assert result == result_11

        result = async_shared_cache.query(async_shared_cache_query_policy, 13)
        assert result == result_13

    def test_async_shared_cache_caches_max_num_slots_results(self):
        threadpool_workers = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())
        threadpool_waiters = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count()*10)
        async_shared_cache = AsyncSharedCache(slots=3, worker_pool=threadpool_workers, waiter_pool=threadpool_waiters)
        func_block_release_event = threading.Event()

        def func(i):
            func_block_release_event.wait()
            return time.time(), i

        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, func)
        async_shared_cache_query_policy.set_result_placeholder(None)
 
        assert async_shared_cache.query(async_shared_cache_query_policy, 17) == None
        assert async_shared_cache.query(async_shared_cache_query_policy, 19) == None
        assert async_shared_cache.query(async_shared_cache_query_policy, 23) == None

        t_before = time.time()
        func_block_release_event.set()
        async_shared_cache_query_policy.delete_result_placeholder()
        result_17 = async_shared_cache.query(async_shared_cache_query_policy, 17)
        result_19 = async_shared_cache.query(async_shared_cache_query_policy, 19)
        result_23 = async_shared_cache.query(async_shared_cache_query_policy, 23)
        t_after = time.time()

        for result, reference_num in ((result_17, 17), (result_19, 19), (result_23, 23)):
            assert result is not None
            assert result[1] == reference_num
            assert result[0] > t_before
            assert result[0] < t_after

        # all ('19', '23' and '17') should be available in cache now
        for num in (19, 23, 17):
            result = async_shared_cache.query(async_shared_cache_query_policy, num)
            assert result is not None
            assert result[1] == num
            assert result[0] > t_before
            assert result[0] < t_after

        # occupy a fourth slot of the AsyncSharedCache will remove the entry '19' as it was least recently accessed
        t_before_27 = time.time()
        result_27 = async_shared_cache.query(async_shared_cache_query_policy, 27)
        t_after_27 = time.time()
        assert result_27 is not None
        assert result_27[1] == 27
        assert result_27[0] > t_before_27
        assert result_27[0] < t_after_27

        async_shared_cache._cache.wait_for_delete_thread()

        result_17 = async_shared_cache.query(async_shared_cache_query_policy, 17)
        assert result_17 is not None
        assert result_17[1] == 17
        assert result_17[0] > t_before
        assert result_17[0] < t_after

        result_23 = async_shared_cache.query(async_shared_cache_query_policy, 23)
        assert result_23 is not None
        assert result_23[1] == 23
        assert result_23[0] > t_before
        assert result_23[0] < t_after

        result_27 = async_shared_cache.query(async_shared_cache_query_policy, 27)
        assert result_27 is not None
        assert result_27[1] == 27
        assert result_27[0] > t_before_27
        assert result_27[0] < t_after_27

        # '19' needs to be executed again as it is not anymore present in cache
        t_before_19 = time.time()
        result_19 = async_shared_cache.query(async_shared_cache_query_policy, 19)
        t_after_19 = time.time()
        assert result_19 is not None
        assert result_19[1] == 19
        assert result_19[0] > t_before_19
        assert result_19[0] < t_after_19

    def test_async_shared_cache_delete_cache_frees_slots(self):
        async_shared_cache = AsyncSharedCache(slots=10)
        async_shared_cache_query_policy = AsyncSharedCacheQueryPolicy(self, lambda val: f"{val}")

        async_shared_cache.query(async_shared_cache_query_policy, 3)
        assert len(async_shared_cache._cache) == 1

        async_shared_cache.delete_cache(self)
        assert len(async_shared_cache._cache) == 0

    def test_async_shared_cache_delete_cache_frees_all_owner_slots(self):
        async_shared_cache = AsyncSharedCache(slots=10)

        owner1 = object()
        owner2 = object()
        owner3 = object()

        async_shared_cache_query_policy1 = AsyncSharedCacheQueryPolicy(owner1, lambda val: f"{val}")
        async_shared_cache_query_policy2 = AsyncSharedCacheQueryPolicy(owner2, lambda val: f"{val}")

        async_shared_cache.query(async_shared_cache_query_policy1, 1)
        async_shared_cache.query(async_shared_cache_query_policy1, 2)
        async_shared_cache.query(async_shared_cache_query_policy1, 3)
        async_shared_cache.query(async_shared_cache_query_policy2, 1)
        async_shared_cache.query(async_shared_cache_query_policy2, 2)

        assert len(async_shared_cache._cache) == 5
        async_shared_cache.delete_cache(owner3)
        assert len(async_shared_cache._cache) == 5
        async_shared_cache.delete_cache(owner1)
        assert len(async_shared_cache._cache) == 2
        async_shared_cache.delete_cache(owner2)
        assert len(async_shared_cache._cache) == 0
