# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import itertools
import logging
import threading
import time

from photoassistant.utils.decoratorutils import async_cached_query
from photoassistant.utils.cachingutils import AsyncSharedCache
from photoassistant.utils.cachingutils import CacheState

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)


class TestAsyncCachedQueryDecorator:
    def setup_method(self, method):
        # This class needs to be defined here as the decorator 'async_cached_query' must be applied freshly for every test case execution
        class Queryable:
            ASYNC_SHARED_CACHE = AsyncSharedCache(slots=3)
            def __init__(self):
                self.queried_event = threading.Event()
            @async_cached_query(ASYNC_SHARED_CACHE)
            def query_test(self, *args, **kwargs):
                # 'query_callback' and 'query_result_placeholder' in kwargs are used for 'async_cached_query'
                try:
                    return (time.time(), args, kwargs)
                finally:
                    self.queried_event.set()
        self.queryable_cls = Queryable

    def test_query_can_be_called_without_callback_or_result_placeholder(self):
        queryable = self.queryable_cls()
        queryable.query_test(1, 2, 3, a=1, b=2, c=3)

    def test_query_can_be_called_without_callback(self):
        queryable = self.queryable_cls()
        queryable.query_test(1, 2, 3, a=1, b=2, c=3, query_result_placeholder=None)

    def test_query_can_be_called_without_result_placeholder(self):
        queryable = self.queryable_cls()
        queryable.query_test(1, 2, 3, a=1, b=2, c=3, query_callback=lambda _: None)

    def test_query_can_be_called_with_result_placeholder_with_callback(self):
        queryable = self.queryable_cls()
        queryable.query_test(1, 2, 3, a=1, b=2, c=3, query_result_placeholder=None, query_callback=lambda _: None)

    def test_query_returns_result_placeholder_when_specified_without_callback(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_result_placeholder="xyz")
        assert result == "xyz"
        assert queryable.queried_event.wait(0.01) == True

    def test_query_returns_result_placeholder_when_specified_with_callback(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_result_placeholder="xyz", query_callback=lambda _: None)
        assert result == "xyz"
        assert queryable.queried_event.wait(0.01) == True

    def test_query_returns_cached_value_when_specified_with_result_placeholder_with_callback(self):
        callback_called_event = threading.Event()
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_result_placeholder="xyz", query_callback=lambda _: callback_called_event.set())
        assert result == "xyz"
        assert queryable.queried_event.wait(0.01) == True
        assert callback_called_event.wait(0.01) == True

        queryable.queried_event.clear()
        callback_called_event.clear()
        result = queryable.query_test("abc", abc=123, query_result_placeholder="xyz", query_callback=lambda _: callback_called_event.set())
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True
        assert callback_called_event.wait(0.01) == True

    def test_query_returns_value_when_specified_without_result_placeholder_without_callback(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) == True

    def test_query_returns_value_when_specified_without_result_placeholder_with_callback(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_callback=lambda _: None)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) == True

    def test_query_returns_cached_value_when_specified_without_result_placeholder_without_callback(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) == True

        queryable.queried_event.clear()
        result = queryable.query_test("abc", abc=123)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True

        result = queryable.query_test("abc", abc=123, query_callback=lambda _: None)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True

    def test_query_returns_cached_value_when_specified_without_result_placeholder_with_callback(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_callback=lambda _: None)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) == True

        queryable.queried_event.clear()
        result = queryable.query_test("abc", abc=123, query_callback=lambda _: None)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True

        result = queryable.query_test("abc", abc=123)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True

    def test_query_executes_callback_with_result_placeholder(self):
        callback_called_event = threading.Event()
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_result_placeholder="xyz", query_callback=lambda _: callback_called_event.set())
        assert result == "xyz"
        assert queryable.queried_event.wait(0.01) == True
        assert callback_called_event.wait(0.01) == True

    def test_query_executes_callback_without_result_placeholder(self):
        callback_called_event = threading.Event()
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123, query_callback=lambda _: callback_called_event.set())
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) == True
        assert callback_called_event.wait(0.01) == True

    def test_query_executes_callback_on_cache_access(self):
        queryable = self.queryable_cls()
        result = queryable.query_test("abc", abc=123)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) == True

        queryable.queried_event.clear()
        result = queryable.query_test("abc", abc=123)
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True

        callback_called_event = threading.Event()
        result = queryable.query_test("abc", abc=123, query_callback=lambda _: callback_called_event.set())
        assert result[1:] == (("abc",), {"abc": 123})
        assert queryable.queried_event.wait(0.01) != True
        assert callback_called_event.wait(0.01) == True

    def test_query_returns_cache_state_if_argument_query_cache_state_only_is_passed_with_true(self):
        queryable = self.queryable_cls()
        assert queryable.query_test("abc", query_cache_state_only=True) == CacheState.DATA_MISSING
        assert queryable.query_test("abc")[1:] == (("abc",), {})
        assert queryable.query_test("abc", query_cache_state_only=True) == CacheState.DATA_READY

    def test_query_fetches_data_again_when_data_is_removed_from_cache(self):
        MAX_WAIT_TIME = 0.001
        queryable = self.queryable_cls()
        queryable.query_test(0)
        queryable.query_test(1)
        queryable.query_test(2)

        # we are using the cached values here
        queryable.queried_event.clear()
        queryable.query_test(0)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) != True
        queryable.query_test(1)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) != True
        queryable.query_test(2)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) != True

        # value for '0' will be removed from cache
        result = queryable.query_test(3)
        queryable.ASYNC_SHARED_CACHE._cache.wait_for_delete_thread()
        assert result[1:] == ((3,), {})
        assert queryable.queried_event.wait(MAX_WAIT_TIME) == True

        # value for '1', '2' and '3' are cached now
        queryable.queried_event.clear()
        queryable.query_test(1)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) != True
        queryable.query_test(2)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) != True
        queryable.query_test(3)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) != True

        # value for '0' needs to be recalculated/fetched again as there is no entry in cache
        queryable.query_test(0)
        assert queryable.queried_event.wait(MAX_WAIT_TIME) == True

    def test_query_works_on_multiple_queryable_objects_adding_newest_and_deleting_oldest_cached_objects_independent_of_owner(self):
        # With three instances of Queryable sharing the same cache, one owner can have 0 elements in cache. This non-trivial case led to a error in the while loop filtering the 'oldest' entry for every owner. (builtin min function can not iterate over empty sequence)
        queryable1 = self.queryable_cls()
        queryable2 = self.queryable_cls()
        queryable3 = self.queryable_cls()

        query_results = []

        # fill all cache slots with objects from owner 'queryable1'
        # (fills query_results[0:3])
        for i in range(3): # 3 slots -> AsyncSharedCache(slots=3)
            query_results.append(queryable1.query_test(i))

        # query the cached slots without adding new objects
        # (fills query_results[3:6])
        for i in range(3): # 3 slots -> AsyncSharedCache(slots=3)
            query_results.append(queryable1.query_test(i))

        # try to cache object for owner 'queryable2' (should delete objects of queryable1)
        # (fills query_results[6:9])
        for i in range(3): # 3 slots -> AsyncSharedCache(slots=3)
            query_results.append(queryable2.query_test(i))
            queryable2.ASYNC_SHARED_CACHE._cache.wait_for_delete_thread()

        # try to cache object for owner 'queryable3' (should delete objects of queryable2)
        # (fills query_results[9:12])
        for i in range(3): # 3 slots -> AsyncSharedCache(slots=3)
            query_results.append(queryable3.query_test(i))
            queryable1.ASYNC_SHARED_CACHE._cache.wait_for_delete_thread()

        # query the cached slot without adding new objects
        # (fills query_results[12])
        query_results.append(queryable3.query_test(2))

        # try to cache an object for owner 'queryable1' again (should delete an object of queryable2)
        # (fills query_results[13])
        query_results.append(queryable1.query_test(1))
        queryable1.ASYNC_SHARED_CACHE._cache.wait_for_delete_thread()

        # based on the assumption of 3 slots -> AsyncSharedCache(slots=3)
        query_result_args = [query_result[1][0] for query_result in query_results]
        assert query_result_args == [0, 1, 2, 0, 1, 2, 0, 1, 2, 0, 1, 2, 2, 1]
        assert query_results[0:3] == query_results[3:6]
        assert query_results[0:3] != query_results[6:9]
        assert query_results[6:9] != query_results[9:12]
        assert query_results[0:3] != query_results[9:12]
        assert query_results[11] == query_results[12]
        assert query_results[1] != query_results[13]
        for owner, query_result in [
                (queryable3, query_results[10]),
                (queryable3, query_results[11]),
                (queryable1, query_results[13]),
            ]:
            async_shared_cache = self.queryable_cls.ASYNC_SHARED_CACHE
            size_restricted_cache = async_shared_cache._cache
            owner_cache = size_restricted_cache._cache[owner]
            cached_return_values = itertools.chain(*[[cache_object.data for cache_object in owner_function_cache.values()] for owner_function_cache in owner_cache.values()])
            assert query_result in cached_return_values
