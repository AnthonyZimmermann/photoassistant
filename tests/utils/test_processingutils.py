# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import logging
import multiprocessing
import multiprocessing.pool
import queue
import time

from photoassistant.utils.processingutils import AsyncExecutePolicy

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

class TestAsyncExecutePolicy:
    def test_async_execute_policy_can_be_created(self):
        AsyncExecutePolicy(lambda: None, lambda _: None)

    def test_async_execute_policy_can_be_created_without_callback(self):
        AsyncExecutePolicy(lambda: None)

    def test_async_execute_policy_executes_policy_async(self):
        # This test uses a queue for collecting results in asynchronously executed callbacks
        # as the queue is a threadsafe container for data. Unpack the data into a list
        # after all async executions submitted their results to the list and assert data.
        async_results = queue.Queue()
        def func(num):
            time.sleep(1e-6 / (1+num))
            return f"{num}"

        def callback(num_string):
            async_results.put(num_string)

        worker_pool = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())

        task_list = sorted(range(100))
        for task in task_list:
            AsyncExecutePolicy(func, callback)(worker_pool, task)

        results = []
        for _ in range(len(task_list)):
            results.append(async_results.get())

        assert all(isinstance(value, str) for value in results)
        results_int = [int(value) for value in results]
        assert task_list != results_int
        assert task_list == sorted(results_int)

    def test_async_execute_policy_executes_policy_async_without_callback(self):
        # This test uses a queue for collecting results in asynchronously executed callbacks
        # as the queue is a threadsafe container for data. Unpack the data into a list
        # after all async executions submitted their results to the list and assert data.
        async_results = queue.Queue()
        def func(num):
            time.sleep(1e-6 / (1+num))
            async_results.put(f"{num}")

        worker_pool = multiprocessing.pool.ThreadPool(multiprocessing.cpu_count())

        task_list = sorted(range(100))
        for task in task_list:
            AsyncExecutePolicy(func)(worker_pool, task)

        results = []
        for _ in range(len(task_list)):
            results.append(async_results.get())

        assert all(isinstance(value, str) for value in results)
        results_int = [int(value) for value in results]
        assert task_list != results_int
        assert task_list == sorted(results_int)
