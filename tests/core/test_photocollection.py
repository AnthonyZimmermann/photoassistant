# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import builtins
import copy
import logging
import os
import shutil
import time
import pytest

from photoassistant.core.metastorage import MetaStorage
from photoassistant.core.photo import Photo
from photoassistant.core.photocollection import PhotoIndex
from photoassistant.core.photocollection import PhotoCollectionManager
from photoassistant.core.photocollection import PhotoCollectionMetaStorageManager
from photoassistant.core.photocollection import PhotoCollectionSettings

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)


class PhotoImplementation(Photo):
    @staticmethod
    def file_is_supported(path):
        return path.endswith(".jpg") and os.path.exists(path)


class TestPhotoIndex:
    def test_instantiation(self):
        PhotoIndex(PhotoImplementation)

    def test_loads_photo_objects(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        assert len(photo_index._photos) == 10

    def test_copy(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        assert len(photo_index._photos) == 10

        photo_index_copied = photo_index.copy()
        assert photo_index_copied != photo_index
        assert photo_index_copied.root_path == photo_index.root_path
        assert id(photo_index_copied._photos) != id(photo_index._photos)
        for photo_a, photo_b in zip(photo_index_copied, photo_index):
            assert photo_a == photo_b

    def test_sort(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        filenames_sorted = [
            '2023-10-13-19-32-06-345-40x30.jpg',
            '2023-10-13-19-32-11-778-40x30.jpg',
            '2023-10-13-19-32-17-102-40x30.jpg',
            '2023-10-13-19-32-25-376-40x30.jpg',
            '2023-10-13-19-32-31-105-40x30.jpg',
            '2023-10-13-19-32-36-602-40x30.jpg',
            '2023-10-13-19-32-40-737-40x30.jpg',
            '2023-10-13-19-32-46-670-40x30.jpg',
            '2023-10-13-19-32-50-307-40x30.jpg',
            '2023-10-13-19-33-03-352.jpg',
        ]

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)

        photo_index.sort(lambda photo: photo.filename)
        assert [photo.filename for photo in photo_index] == filenames_sorted

        # inverting the ascii character value for every character should give reversed order
        # when sorting
        photo_index.sort(lambda photo: list((256 - ord(c)) for c in photo.filename))
        assert [photo.filename for photo in photo_index] == list(reversed(filenames_sorted))

    def test_loads_photo_objects_with_sort_key(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        filenames_sorted = [
            '2023-10-13-19-32-06-345-40x30.jpg',
            '2023-10-13-19-32-11-778-40x30.jpg',
            '2023-10-13-19-32-17-102-40x30.jpg',
            '2023-10-13-19-32-25-376-40x30.jpg',
            '2023-10-13-19-32-31-105-40x30.jpg',
            '2023-10-13-19-32-36-602-40x30.jpg',
            '2023-10-13-19-32-40-737-40x30.jpg',
            '2023-10-13-19-32-46-670-40x30.jpg',
            '2023-10-13-19-32-50-307-40x30.jpg',
            '2023-10-13-19-33-03-352.jpg',
        ]

        sort_key = lambda photo: photo.filename
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path, sort_key=sort_key)
        assert [photo.filename for photo in photo_index] == filenames_sorted

        # inverting the ascii character value for every character should give reversed order
        # when sorting
        sort_key = lambda photo: list((256 - ord(c)) for c in photo.filename)
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path, sort_key=sort_key)
        assert [photo.filename for photo in photo_index] == list(reversed(filenames_sorted))

    @pytest.mark.skip(reason="Implementation got slow for unknown reason, overhead of the overlay file access might not be only the reason")
    def test_loads_10_photo_objects_in_less_than_100us(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")

        t0 = time.time()
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        t_load = time.time() - t0

        assert (t_load) < 10e-6
        assert len(photo_index._photos) == 10
        assert len(photo_index) == 10

    @pytest.mark.skip(reason="Impelementation got slow a second time for unknown reason")
    def test_loads_10_photo_objects_in_less_than_4ms(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")

        t0 = time.time()
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        t_load = time.time() - t0

        assert (t_load) < 4e-3
        assert len(photo_index._photos) == 10
        assert len(photo_index) == 10

    def test_loads_10_photo_objects_in_less_than_20ms(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")

        t0 = time.time()
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        t_load = time.time() - t0

        assert (t_load) < 20e-3
        assert len(photo_index._photos) == 10
        assert len(photo_index) == 10

    def test_index(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)

        assert len(photo_index._photos) == 10
        assert len(photo_index) == 10
        photos = [photo for photo in photo_index]

        indexed_photos = list(enumerate(photos))
        indexed_photos = [indexed_photos[i] for i in [5, 2, 6, 3, 4, 7, 8, 1, 0, 9]]
        for i, photo in indexed_photos:
            assert photo_index.index(photo) == i

    def test_remove(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)

        assert len(photo_index._photos) == 10
        assert len(photo_index) == 10
        photos = [photo for photo in photo_index]

        photo_delete_test = photos.pop(3)
        photo_delete_test_path = photo_delete_test.get_path()
        photo_index.remove(photo_delete_test)

        assert len(photo_index._photos) == 9
        assert len(photo_index) == 9
        new_photos = [photo for photo in photo_index]

        assert photos == new_photos
        assert photo_delete_test_path not in photo_index._photos

    def test_sequence_interface(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)

        filenames = [
            "2023-10-13-19-32-06-345-40x30.jpg",
            "2023-10-13-19-32-11-778-40x30.jpg",
            "2023-10-13-19-32-17-102-40x30.jpg",
            "2023-10-13-19-32-36-602-40x30.jpg",
            "2023-10-13-19-32-40-737-40x30.jpg",
            "2023-10-13-19-32-25-376-40x30.jpg",
            "2023-10-13-19-32-31-105-40x30.jpg",
            "2023-10-13-19-32-46-670-40x30.jpg",
            "2023-10-13-19-32-50-307-40x30.jpg",
            "2023-10-13-19-33-03-352.jpg",
        ]

        assert photo_index[3].filename in filenames
        assert photo_index[-1].filename in filenames

        assert isinstance(photo_index[2:6:2], list)

        for photo in photo_index[2:6:2]:
            assert photo.filename in filenames

        for photo in photo_index:
            assert photo.filename in filenames
            assert photo.main_tag is None
            filenames.pop(filenames.index(photo.filename))

        assert len(filenames) == 0

    def test_iterable_interface(self):
        photo_index_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)

        filenames = [
            "2023-10-13-19-32-06-345-40x30.jpg",
            "2023-10-13-19-32-11-778-40x30.jpg",
            "2023-10-13-19-32-17-102-40x30.jpg",
            "2023-10-13-19-32-36-602-40x30.jpg",
            "2023-10-13-19-32-40-737-40x30.jpg",
            "2023-10-13-19-32-25-376-40x30.jpg",
            "2023-10-13-19-32-31-105-40x30.jpg",
            "2023-10-13-19-32-46-670-40x30.jpg",
            "2023-10-13-19-32-50-307-40x30.jpg",
            "2023-10-13-19-33-03-352.jpg",
        ]

        for photo in (p for p in photo_index):
            assert photo.filename in filenames
            assert photo.main_tag is None
            filenames.pop(filenames.index(photo.filename))

        assert len(filenames) == 0


class TestPhotoCollectionSettings:
    def test_instantiation(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        PhotoCollectionSettings("1.0", photo_collection_settings_path)

    def test_instantiation_raises_exception_if_path_does_not_exist(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "does-not-exist")
        with pytest.raises(AssertionError):
            PhotoCollectionSettings("1.0", photo_collection_settings_path)

    def test_instantiation_raises_exception_if_path_is_not_a_directory(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "photocollection", "collection.config")
        with pytest.raises(AssertionError):
            PhotoCollectionSettings("1.0", photo_collection_settings_path)

    def test_setting_tagged_photos_root_folder(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        assert settings.tagged_photos_root_folder == ""
        settings.tagged_photos_root_folder = "tests"
        assert settings.tagged_photos_root_folder == "tests"

    def test_setting_tagged_photos_root_folder_raises_value_error_on_invalid_value(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        assert settings.tagged_photos_root_folder == ""
        with pytest.raises(ValueError):
            settings.tagged_photos_root_folder = "/tests"
        with pytest.raises(ValueError):
            settings.tagged_photos_root_folder = "tests/"
        with pytest.raises(ValueError):
            settings.tagged_photos_root_folder = "te/sts"
        with pytest.raises(ValueError):
            settings.tagged_photos_root_folder = r"te\sts"

    def test_store_default_settings(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)

        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        settings.store()

        photo_collection_settings_file_path = os.path.join(photo_collection_settings_path, PhotoCollectionSettings.FILENAME)
        assert builtins.open.calls == [((photo_collection_settings_file_path, "w"), {})]
        expected_content = os.linesep.join([
            "version=1.0",
            "deleted_photos_folder = deleted",
            "tagged_photos_root_folder = \"\"",
        ]).encode()
        assert file_access_patcher.overlay.get(photo_collection_settings_file_path).content == expected_content

    def test_store_modified_settings(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)

        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        settings.tagged_photos_root_folder = "collection"
        settings.deleted_photos_folder = "trashcan"
        settings.store()

        photo_collection_settings_file_path = os.path.join(photo_collection_settings_path, PhotoCollectionSettings.FILENAME)
        assert builtins.open.calls == [((photo_collection_settings_file_path, "w"), {})]
        expected_content = os.linesep.join([
            "version=1.0",
            "deleted_photos_folder = trashcan",
            "tagged_photos_root_folder = collection",
        ]).encode()
        assert file_access_patcher.overlay.get(photo_collection_settings_file_path).content == expected_content

    def test_store_settings_fails_when_tagged_photos_root_folder_and_deleted_photos_folder_are_same(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        settings.tagged_photos_root_folder = "abc"
        settings.deleted_photos_folder = "abc"
        with pytest.raises(AssertionError):
            settings.store()

    def test_load_settings(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "photocollection")
        settings = PhotoCollectionSettings.load(photo_collection_settings_path)
        assert settings.tagged_photos_root_folder == "autosorted"
        assert settings.deleted_photos_folder == "garbage"

    def test_load_settings_fails_when_tagged_photos_root_folder_and_deleted_photos_folder_are_same(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "photocollection")
        settings_data = (
            f"version=1.0{os.linesep}"
            f"tagged_photos_root_folder = collection{os.linesep}"
            f"deleted_photos_folder = collection{os.linesep}"
        )
        photo_collection_settings_file_path = os.path.join(
            photo_collection_settings_path,
            PhotoCollectionSettings.FILENAME,
        )
        with open(photo_collection_settings_file_path, "w") as file_:
            file_.write(settings_data)
        with pytest.raises(AssertionError):
            PhotoCollectionSettings.load(photo_collection_settings_path)

    def test_load_modify_store_and_load_again(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)

        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "photocollection")
        photo_collection_settings_file_path = os.path.join(photo_collection_settings_path, PhotoCollectionSettings.FILENAME)

        settings = PhotoCollectionSettings.load(photo_collection_settings_path)
        assert settings.tagged_photos_root_folder == "autosorted"
        assert settings.deleted_photos_folder == "garbage"

        settings.tagged_photos_root_folder = "tagged"
        settings.deleted_photos_folder = "trash"
        settings.store()

        assert builtins.open.calls == [
            ((photo_collection_settings_file_path, "r"), {}),
            ((photo_collection_settings_file_path, "w"), {})
        ]
        expected_content = os.linesep.join([
            "version=1.0",
            "deleted_photos_folder = trash",
            "tagged_photos_root_folder = tagged",
        ]).encode()
        assert file_access_patcher.overlay.get(photo_collection_settings_file_path).content == expected_content
 
        settings = PhotoCollectionSettings.load(photo_collection_settings_path)
        assert builtins.open.calls[-1] == ((photo_collection_settings_file_path, "r"), {})
        assert settings.tagged_photos_root_folder == "tagged"
        assert settings.deleted_photos_folder == "trash"

    def test_assert_default_settings(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        settings.assert_settings()

    def test_assert_settings_fails_when_tagged_photos_root_folder_and_deleted_photos_folder_are_same(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        settings = PhotoCollectionSettings("1.0", photo_collection_settings_path)
        settings.tagged_photos_root_folder = "abc"
        settings.deleted_photos_folder = "abc"
        with pytest.raises(AssertionError):
            settings.assert_settings()

    def test_find_settings_files_find_multiple_files(self):
        photo_collection_settings_1 = pytest.utils.get_test_resources_path("photos", "photocollection")
        photo_collection_settings_2 = os.path.join(photo_collection_settings_1, "autogenerated-directory")
        os.mkdir(photo_collection_settings_2)
        photo_collection_settings_1_path = os.path.join(photo_collection_settings_1, PhotoCollectionSettings.FILENAME)
        photo_collection_settings_2_path = os.path.join(photo_collection_settings_2, PhotoCollectionSettings.FILENAME)
        shutil.copy(photo_collection_settings_1_path, photo_collection_settings_2_path)

        paths = PhotoCollectionSettings._find_settings_files(photo_collection_settings_1)
        assert sorted(paths) == sorted([photo_collection_settings_1_path, photo_collection_settings_2_path])

        paths = PhotoCollectionSettings._find_settings_files(photo_collection_settings_2)
        assert sorted(paths) == sorted([photo_collection_settings_1_path, photo_collection_settings_2_path])

        paths = PhotoCollectionSettings._find_settings_files(os.path.dirname(photo_collection_settings_1))
        assert sorted(paths) == sorted([photo_collection_settings_1_path, photo_collection_settings_2_path])

        paths = PhotoCollectionSettings._find_settings_files(os.path.dirname(os.path.dirname(photo_collection_settings_1)))
        assert sorted(paths) == sorted([photo_collection_settings_1_path, photo_collection_settings_2_path])

    def test_find_settings_files_in_same_directory(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "photocollection")
        photo_collection_settings_file_path = pytest.utils.get_test_resources_path("photos", "photocollection", PhotoCollectionSettings.FILENAME)
        paths = PhotoCollectionSettings._find_settings_files(photo_collection_settings_path)
        assert len(paths) == 1
        path = paths[0]
        assert path == photo_collection_settings_file_path

    def test_find_settings_files_in_any_parent_directory(self):
        photo_collection_settings_path_1 = pytest.utils.get_test_resources_path("photos", "photocollection", "d1")
        photo_collection_settings_path_2 = pytest.utils.get_test_resources_path("photos", "photocollection", "d1", "d1_3", "d1_3_2")
        photo_collection_settings_file_path = pytest.utils.get_test_resources_path("photos", "photocollection", PhotoCollectionSettings.FILENAME)
        paths = PhotoCollectionSettings._find_settings_files(photo_collection_settings_path_1)
        assert len(paths) == 1
        path = paths[0]
        assert path == photo_collection_settings_file_path

        paths = PhotoCollectionSettings._find_settings_files(photo_collection_settings_path_2)
        assert len(paths) == 1
        path = paths[0]
        assert path == photo_collection_settings_file_path

    def test_find_settings_file_not_present(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        paths = PhotoCollectionSettings._find_settings_files(photo_collection_settings_path)
        assert len(paths) == 0


class TestPhotoCollectionMetaStorageManager:
    def test_migrate_meta_data_adds_meta_data_to_destination_meta_storage(self):
        source_meta_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        destination_meta_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_multiple_meta_files")
        source_meta_storage = MetaStorage(source_meta_path)
        # use set_meta to make sure that source meta data will have a fresh update-timestamp
        # (fresher than destination)
        source_meta_storage.set_meta("hash1", "filename-abc", {"test-meta": "abc"})

        source_meta_storage_object = copy.deepcopy(MetaStorage(source_meta_path)._data["hash1"])
        destination_meta_storage_object = copy.deepcopy(MetaStorage(destination_meta_path)._data["hash1"])
        assert destination_meta_storage_object.filename != source_meta_storage_object.filename
        assert destination_meta_storage_object.meta != source_meta_storage_object.meta

        # migrate destination data (update timestamp later) into destination meta_storage
        PhotoCollectionMetaStorageManager.migrate_meta_data("hash1", source_meta_path, destination_meta_path)
        destination_meta_storage_object = copy.deepcopy(MetaStorage(destination_meta_path)._data["hash1"])
        assert destination_meta_storage_object.filename == source_meta_storage_object.filename
        assert destination_meta_storage_object.meta == source_meta_storage_object.meta

    def test_migrate_meta_data_keeps_destination_meta_data_if_source_meta_data_is_older(self):
        destination_meta_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        source_meta_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_multiple_meta_files")
        destination_meta_storage = MetaStorage(destination_meta_path)
        # use set_meta to make sure that destination meta data will have a fresh update-timestamp
        # (fresher than source)
        destination_meta_storage.set_meta("hash1", "filename-abc", {"test-meta": "abc"})

        destination_meta_storage_object = copy.deepcopy(MetaStorage(destination_meta_path)._data["hash1"])
        source_meta_storage_object = copy.deepcopy(MetaStorage(source_meta_path)._data["hash1"])
        assert source_meta_storage_object.filename != destination_meta_storage_object.filename
        assert source_meta_storage_object.meta != destination_meta_storage_object.meta

        # do not migrate destination data (update timestamp earlier) back into destination_meta_storage
        PhotoCollectionMetaStorageManager.migrate_meta_data("hash1", source_meta_path, destination_meta_path)
        destination_meta_storage_object = copy.deepcopy(MetaStorage(destination_meta_path)._data["hash1"])
        assert destination_meta_storage_object.filename != source_meta_storage_object.filename
        assert destination_meta_storage_object.meta != source_meta_storage_object.meta
        assert destination_meta_storage_object.filename == "filename-abc"
        assert destination_meta_storage_object.meta == {"test-meta": "abc"}

    def test_migrate_meta_data_removes_meta_data_from_source_meta_storage(self):
        source_meta_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        destination_meta_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_multiple_meta_files")
        source_meta_storage = MetaStorage(source_meta_path)
        source_meta_storage.set_meta("hash1", "filename-abc", {"test-meta": "abc"})

        source_meta_storage_object = copy.deepcopy(MetaStorage(source_meta_path)._data["hash1"])
        destination_meta_storage_object = copy.deepcopy(MetaStorage(destination_meta_path)._data["hash1"])
        assert destination_meta_storage_object.filename != source_meta_storage_object.filename
        assert destination_meta_storage_object.meta != source_meta_storage_object.meta

        # migrate destination data (update timestamp later) into destination meta_storage
        PhotoCollectionMetaStorageManager.migrate_meta_data("hash1", source_meta_path, destination_meta_path)
        assert "hash1" not in MetaStorage(source_meta_path)._data

    def test_migrate_meta_data_with_same_source_and_target_directory(self):
        meta_storage_directory = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(meta_storage_directory)
        meta_storage_path = meta_storage._meta_path
        assert os.path.exists(meta_storage._meta_path)
        assert isinstance(meta_storage_path, str)

        with open(meta_storage_path, "r") as file_:
            original_content = file_.read()

        PhotoCollectionMetaStorageManager.migrate_meta_data("hash1", meta_storage_directory, meta_storage_directory)
        PhotoCollectionMetaStorageManager.migrate_meta_data("hash2", meta_storage_directory, meta_storage_directory)
        meta_storage_hash_3 = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"
        PhotoCollectionMetaStorageManager.migrate_meta_data(meta_storage_hash_3, meta_storage_directory, meta_storage_directory)

        with open(meta_storage_path, "r") as file_:
            modified_content = file_.read()

        assert modified_content == original_content

    def test_find_all_directories_with_meta_files_for_whole_photocollection(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files")
        photo_collection_meta_storage_manager = PhotoCollectionMetaStorageManager(collection_root)

        meta_files = photo_collection_meta_storage_manager.find_all_directories_with_meta_files()

        expected_meta_files = [
            "with_autoformatted_meta_file",
            "with_meta_file",
            "collection_meta_storage_manager",
            "collection_meta_storage_manager/collection-subdir",
            "with_multiple_meta_files",
            "empty_meta_file",
        ]
        expected_meta_files = sorted(os.path.join(collection_root, f) for f in expected_meta_files)

        assert sorted(meta_files) == expected_meta_files

    def test_find_all_directories_with_meta_files_for_collection_subdir(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files")
        photo_collection_meta_storage_manager = PhotoCollectionMetaStorageManager(collection_root)

        meta_files = photo_collection_meta_storage_manager.find_all_directories_with_meta_files(os.path.join(collection_root, "collection_meta_storage_manager"))

        expected_meta_files = [
            "collection_meta_storage_manager",
            "collection_meta_storage_manager/collection-subdir",
        ]
        expected_meta_files = sorted(os.path.join(collection_root, f) for f in expected_meta_files)
        assert sorted(meta_files) == expected_meta_files

    def test_cleanup_meta_data_and_directories_removes_meta_data_from_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        photo_collection_meta_storage_manager = PhotoCollectionMetaStorageManager(collection_root)
        assert "hash1" in MetaStorage(photo_index_path)._data
        assert "hash2" in MetaStorage(photo_index_path)._data

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        photo_collection_meta_storage_manager._cleanup_meta_data_and_directories(photo_index)

        assert "hash1" not in MetaStorage(photo_index_path)._data
        assert "hash2" not in MetaStorage(photo_index_path)._data

    def test_cleanup_meta_data_and_directories_keeps_meta_data_in_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        photo_collection_meta_storage_manager = PhotoCollectionMetaStorageManager(collection_root)
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        assert photo_hash in MetaStorage(photo_index_path)._data

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        photo_collection_meta_storage_manager._cleanup_meta_data_and_directories(photo_index)
        assert photo_hash in MetaStorage(photo_index_path)._data

    def test_cleanup_meta_data_and_directories_writes_meta_data_to_collection_root_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        photo_collection_meta_storage_manager = PhotoCollectionMetaStorageManager(collection_root)
        assert "hash1" in MetaStorage(photo_index_path)._data
        assert "hash2" in MetaStorage(photo_index_path)._data
        assert "hash1" not in MetaStorage(collection_root)._data
        assert "hash2" not in MetaStorage(collection_root)._data

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        photo_collection_meta_storage_manager._cleanup_meta_data_and_directories(photo_index)

        assert "hash1" not in MetaStorage(photo_index_path)._data
        assert "hash2" not in MetaStorage(photo_index_path)._data
        assert "hash1" in MetaStorage(collection_root)._data
        assert "hash2" in MetaStorage(collection_root)._data

    def test_cleanup_meta_data_and_directories_updates_meta_data_in_collection_root_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager", "collection-subdir")

        MetaStorage(photo_index_path).set_meta("hash1", "photo-abc", {"abc-meta": "abcde"})
        assert MetaStorage(collection_root)._data["hash1"].filename != "photo-abc"
        assert MetaStorage(collection_root)._data["hash1"].meta != {"abc-meta": "abcde"}

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        PhotoCollectionMetaStorageManager(collection_root)._cleanup_meta_data_and_directories(photo_index)

        assert MetaStorage(collection_root)._data["hash1"].filename == "photo-abc"
        assert MetaStorage(collection_root)._data["hash1"].meta == {"abc-meta": "abcde"}

    def test_cleanup_meta_data_and_directories_does_not_update_meta_data_in_collection_root_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager", "collection-subdir")

        MetaStorage(collection_root).set_meta("hash1", "photo-abc", {"abc-meta": "abcde"})
        assert MetaStorage(photo_index_path)._data["hash1"].filename != "photo-abc"
        assert MetaStorage(photo_index_path)._data["hash1"].meta != {"abc-meta": "abcde"}

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        PhotoCollectionMetaStorageManager(collection_root)._cleanup_meta_data_and_directories(photo_index)

        assert MetaStorage(collection_root)._data["hash1"].filename == "photo-abc"
        assert MetaStorage(collection_root)._data["hash1"].meta == {"abc-meta": "abcde"}

    def test_migrate_collection_root_meta_data_updates_meta_storage_from_collection_root_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager", "collection-subdir")
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        MetaStorage(collection_root).set_meta(photo_hash, "photo-abc", {"abc-meta": "abcde"})
        assert MetaStorage(photo_index_path)._data[photo_hash].filename != "photo-abc"
        assert MetaStorage(photo_index_path)._data[photo_hash].meta != {"abc-meta": "abcde"}

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        PhotoCollectionMetaStorageManager(collection_root)._migrate_collection_root_meta_data(photo_index)

        assert MetaStorage(photo_index_path)._data[photo_hash].filename == "photo-abc"
        assert MetaStorage(photo_index_path)._data[photo_hash].meta == {"abc-meta": "abcde"}

    def test_migrate_collection_root_meta_data_does_not_update_meta_storage_from_collection_root_meta_storage(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager", "collection-subdir")
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        MetaStorage(photo_index_path).set_meta(photo_hash, "photo-abc", {"abc-meta": "abcde"})
        wrong_filename = copy.deepcopy(MetaStorage(collection_root)._data[photo_hash].filename)
        wrong_meta = copy.deepcopy(MetaStorage(collection_root)._data[photo_hash].meta)
        correct_filename = copy.deepcopy(MetaStorage(photo_index_path)._data[photo_hash].filename)
        correct_meta = copy.deepcopy(MetaStorage(photo_index_path)._data[photo_hash].meta)

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        PhotoCollectionMetaStorageManager(collection_root)._migrate_collection_root_meta_data(photo_index)

        assert MetaStorage(photo_index_path)._data[photo_hash].filename != wrong_filename
        assert MetaStorage(photo_index_path)._data[photo_hash].meta != wrong_meta
        assert MetaStorage(photo_index_path)._data[photo_hash].filename == correct_filename
        assert MetaStorage(photo_index_path)._data[photo_hash].meta == correct_meta

    def test_cleanup_and_migrate_meta_data_and_directories(self):
        collection_root = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager")
        photo_index_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager", "collection-subdir")
        photos_new_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "collection_meta_storage_manager", "collection-subdir", "inner-subdir")
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        shutil.move(os.path.join(photo_index_path, "2023-10-13-19-32-25-376-40x30.jpg"), photos_new_path)
        shutil.move(os.path.join(photo_index_path, "2023-10-13-19-32-31-105-40x30.jpg"), photos_new_path)
        shutil.move(os.path.join(photo_index_path, "2023-10-13-19-32-40-737-40x30.jpg"), photos_new_path)

        assert "hash1" in MetaStorage(collection_root)._data
        assert "hash2" in MetaStorage(collection_root)._data
        assert photo_hash in MetaStorage(collection_root)._data
        assert "hash1" in MetaStorage(photo_index_path)._data
        assert "hash2" in MetaStorage(photo_index_path)._data
        assert photo_hash in MetaStorage(photo_index_path)._data
        assert "hash1" not in MetaStorage(photos_new_path)._data
        assert "hash2" not in MetaStorage(photos_new_path)._data
        assert photo_hash not in MetaStorage(photos_new_path)._data

        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(photo_index_path)
        PhotoCollectionMetaStorageManager(collection_root).cleanup_and_migrate_meta_data_and_directories(photo_index)

        assert "hash1" in MetaStorage(collection_root)._data
        assert "hash2" in MetaStorage(collection_root)._data
        assert photo_hash not in MetaStorage(collection_root)._data
        assert "hash1" not in MetaStorage(photo_index_path)._data
        assert "hash2" not in MetaStorage(photo_index_path)._data
        assert photo_hash not in MetaStorage(photo_index_path)._data
        assert "hash1" not in MetaStorage(photos_new_path)._data
        assert "hash2" not in MetaStorage(photos_new_path)._data
        assert photo_hash in MetaStorage(photos_new_path)._data


class PhotoCollectionManagerTestBase:
    PHOTO_COLLECTION_PATH = None

    def test_initialization(self):
        PhotoCollectionManager(PhotoImplementation)

    def test_set_collection_root(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        assert photo_collection.collection_root is None
        assert photo_collection.meta_storage_manager is None
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.collection_root == self.PHOTO_COLLECTION_PATH
        assert isinstance(photo_collection.meta_storage_manager, PhotoCollectionMetaStorageManager)

    def test_collection_settings_present_in_collection_root_after_initialization(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.collection_root is not None
        assert os.path.exists(os.path.join(photo_collection.collection_root, PhotoCollectionSettings.FILENAME))

    def test_open_photos_in_collection_root(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert isinstance(photo_collection.photo_index, PhotoIndex)

    def test_open_photos_within_collection_root(self):
        assert self.PHOTO_COLLECTION_PATH is not None
        subdirectory_path = os.path.join(self.PHOTO_COLLECTION_PATH, "autocreated-testdir")
        os.mkdir(subdirectory_path)
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(subdirectory_path)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert isinstance(photo_collection.photo_index, PhotoIndex)

    def test_open_photos_outside_collection_root(self):
        photo_collection_root_path = PhotoCollectionManager.find_collection(self.PHOTO_COLLECTION_PATH)
        assert photo_collection_root_path is not None
        sibling_directory_path = os.path.join(os.path.dirname(photo_collection_root_path), "autocreated-testdir")
        os.mkdir(sibling_directory_path)
        PhotoCollectionManager.create(sibling_directory_path)
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(sibling_directory_path)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert isinstance(photo_collection.photo_index, PhotoIndex)

    def base_test_photo_index_number_of_images(self, number_of_images_expected):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        assert len(photo_collection.photo_index) == number_of_images_expected

    def base_test_move_file_with_set_path(self, old_path, new_path):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        photo = next(filter(lambda p: p.path == old_path, photo_collection.photo_index), None)
        assert photo is not None

        assert photo.path == old_path
        photo.set_path(new_path)
        return photo

    def base_test_set_main_tag(self, photo_path, tag):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        photo = next(filter(lambda p: p.path == photo_path, photo_collection.photo_index), None)
        assert photo is not None
        photo.set_main_tag(tag)
        assert photo.main_tag == tag
        return photo


class TestPhotoCollectionManagerNewEmptyCollection(PhotoCollectionManagerTestBase):
    PHOTO_COLLECTION_PATH = pytest.utils.get_test_resources_path("photos", "empty_directory")

    def setup_method(self):
        assert PhotoCollectionManager.find_collection(self.PHOTO_COLLECTION_PATH) is None
        PhotoCollectionManager.create(self.PHOTO_COLLECTION_PATH)

    def test_create_collection(self):
        sibling_collection = os.path.join(os.path.dirname(self.PHOTO_COLLECTION_PATH), "autogenerated-sibling-empty-directory")
        os.mkdir(sibling_collection)
        PhotoCollectionManager.create(sibling_collection)
        assert PhotoCollectionManager.find_collection(sibling_collection) == sibling_collection

    def test_set_collection_root_fails_when_directory_does_not_exist(self):
        non_existend_path = f"{self.PHOTO_COLLECTION_PATH}_x"
        assert not os.path.exists(non_existend_path)
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        with pytest.raises(AssertionError):
            photo_collection.set_collection_root(non_existend_path)

    def test_photo_index_number_of_images(self):
        super().base_test_photo_index_number_of_images(0)


class TestPhotoCollectionManagerNewCollection(PhotoCollectionManagerTestBase):
    PHOTO_COLLECTION_PATH = pytest.utils.get_test_resources_path("photos", "sorted_10")

    def setup_method(self):
        assert PhotoCollectionManager.find_collection(self.PHOTO_COLLECTION_PATH) is None
        PhotoCollectionManager.create(self.PHOTO_COLLECTION_PATH)

    def test_find_collection(self):
        # In a fresh sibling directory, there should be no collection present
        sibling_directory = os.path.join(os.path.dirname(self.PHOTO_COLLECTION_PATH), "autogenerated-sibling-empty-directory")
        os.mkdir(sibling_directory)
        assert PhotoCollectionManager.find_collection(sibling_directory) is None

    def test_photo_index_number_of_images(self):
        super().base_test_photo_index_number_of_images(10)

    def test_move_file_with_set_path(self, file_access_patcher):
        old_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        new_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "2023-10-13-19-32-06-345-40x30.jpg")
        assert os.path.exists(old_path)
        assert not os.path.exists(new_path) # not yet

        photo = super().base_test_move_file_with_set_path(old_path, new_path)
        assert photo.path == new_path
        assert (file_access_patcher.overlay.get(old_path).original_content
                == file_access_patcher.overlay.get(new_path).content)
        assert not os.path.exists(old_path) # not anymore
        assert os.path.exists(new_path)

    def test_move_file_with_set_path_does_nothing_if_the_current_path_is_passed(self, file_access_patcher):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        assert os.path.exists(photo_path)
        photo_file_content = file_access_patcher.overlay.get(photo_path).content
        super().base_test_move_file_with_set_path(photo_path, photo_path)
        assert file_access_patcher.overlay.get(photo_path).content == photo_file_content

    def test_move_file_with_set_path_does_not_overwrite_another_photo(self, file_access_patcher):
        old_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        new_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "2023-10-13-19-32-17-102-40x30.jpg")
        actual_new_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "2023-10-13-19-32-17-102-40x30--1.jpg")
        assert os.path.exists(old_path)
        assert os.path.exists(new_path)
        assert not os.path.exists(actual_new_path) # not yet
        photo = super().base_test_move_file_with_set_path(old_path, new_path)
        assert photo.path != new_path
        assert photo.path == actual_new_path
        assert (file_access_patcher.overlay.get(old_path).original_content
                != file_access_patcher.overlay.get(new_path).content)
        assert (file_access_patcher.overlay.get(old_path).original_content
                == file_access_patcher.overlay.get(actual_new_path).content)
        assert not os.path.exists(old_path) # not anymore
        assert os.path.exists(new_path)
        assert os.path.exists(actual_new_path)

    def test_move_file_with_set_path_creates_proper_file_name_suffix(self, file_access_patcher):
        old_path_1 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        old_path_2 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "d1_3_2", "2023-10-13-19-32-25-376-40x30.jpg")
        old_path_3 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "d1_3_2", "2023-10-13-19-32-31-105-40x30.jpg")
        new_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "moved-1.jpg")
        actual_new_path_2 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "moved-1--1.jpg")
        actual_new_path_3 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "moved-1--2.jpg")

        photo_1 = super().base_test_move_file_with_set_path(old_path_1, new_path)
        photo_2 = super().base_test_move_file_with_set_path(old_path_2, new_path)
        photo_3 = super().base_test_move_file_with_set_path(old_path_3, new_path)

        assert photo_1.path == new_path
        assert photo_2.path == actual_new_path_2
        assert photo_3.path == actual_new_path_3
        assert (file_access_patcher.overlay.get(old_path_1).original_content
                == file_access_patcher.overlay.get(new_path).content)
        assert (file_access_patcher.overlay.get(old_path_2).original_content
                == file_access_patcher.overlay.get(actual_new_path_2).content)
        assert (file_access_patcher.overlay.get(old_path_3).original_content
                == file_access_patcher.overlay.get(actual_new_path_3).content)

    def test_move_file_with_set_path_creates_directory(self, file_access_patcher):
        old_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        new_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1_new", "moved.jpg")
        assert os.path.exists(old_path)
        assert not os.path.exists(new_path) # not yet
        assert not os.path.exists(os.path.dirname(new_path)) # not yet

        photo = super().base_test_move_file_with_set_path(old_path, new_path)
        assert photo.path == new_path
        assert (file_access_patcher.overlay.get(old_path).original_content
                == file_access_patcher.overlay.get(new_path).content)

        assert not os.path.exists(old_path) # not anymore
        assert os.path.exists(os.path.dirname(new_path))
        assert os.path.exists(new_path)

    def test_move_file_with_set_path_deletes_empty_directory(self, file_access_patcher):
        pytest.utils.delete_all_license_files_from_resources() 
        old_path_1 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "d1_3_2", "2023-10-13-19-32-25-376-40x30.jpg")
        old_path_2 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "d1_3_2", "2023-10-13-19-32-31-105-40x30.jpg")
        new_path_1 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "moved-1.jpg")
        new_path_2 = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "d1_3", "moved-2.jpg")

        assert all(os.path.exists(p) for p in [old_path_1, old_path_2])
        assert not any(os.path.exists(p) for p in [new_path_1, new_path_2])

        photo_1 = super().base_test_move_file_with_set_path(old_path_1, new_path_1)
        assert photo_1.path == new_path_1
        assert (file_access_patcher.overlay.get(old_path_1).original_content
                == file_access_patcher.overlay.get(new_path_1).content)
        assert all(os.path.exists(p) for p in [new_path_1, old_path_2])
        assert not any(os.path.exists(p) for p in [old_path_1, new_path_2])

        photo_2 = super().base_test_move_file_with_set_path(old_path_2, new_path_2)
        assert photo_2.path == new_path_2
        assert (file_access_patcher.overlay.get(old_path_2).original_content
                == file_access_patcher.overlay.get(new_path_2).content)
        assert all(os.path.exists(p) for p in [new_path_1, new_path_2])
        assert not any(os.path.exists(p) for p in [old_path_1, old_path_2])

        assert not os.path.exists(os.path.dirname(old_path_1))

    def test_set_main_tag_moves_file(self, file_access_patcher):
        tagged_photos_root_folder = "" # default value of PhotoCollectionSettings.tagged_photos_root_folder
        old_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        new_path = pytest.utils.get_test_resources_path("photos", "sorted_10", tagged_photos_root_folder, "test-tag", "abc", "2023-10-13-19-32-06-345-40x30.jpg")

        assert os.path.exists(old_path)
        assert not os.path.exists(new_path) # not yet
        assert not os.path.exists(os.path.dirname(new_path)) # not yet

        photo = super().base_test_set_main_tag(old_path, ":test-tag:abc")
        assert photo.path == new_path
        assert (file_access_patcher.overlay.get(old_path).original_content
                == file_access_patcher.overlay.get(new_path).content)

        assert not os.path.exists(old_path) # not anymore
        assert os.path.exists(os.path.dirname(new_path))
        assert os.path.exists(new_path)


class TestPhotoCollectionManagerPreExisting(PhotoCollectionManagerTestBase):
    PHOTO_COLLECTION_PATH = pytest.utils.get_test_resources_path("photos", "photocollection")

    def test_find_collection(self):
        assert PhotoCollectionManager.find_collection(self.PHOTO_COLLECTION_PATH) == self.PHOTO_COLLECTION_PATH

    def test_collection_raises_exception_on_multiple_settings_files_found(self):
        photo_collection_settings_1 = pytest.utils.get_test_resources_path("photos", "photocollection")
        photo_collection_settings_2 = os.path.join(photo_collection_settings_1, "autogenerated-directory")
        os.mkdir(photo_collection_settings_2)
        photo_collection_settings_1_path = os.path.join(photo_collection_settings_1, PhotoCollectionSettings.FILENAME)
        photo_collection_settings_2_path = os.path.join(photo_collection_settings_2, PhotoCollectionSettings.FILENAME)
        shutil.copy(photo_collection_settings_1_path, photo_collection_settings_2_path)

        with pytest.raises(PhotoCollectionManager.CollectionError):
            PhotoCollectionManager.find_collection(photo_collection_settings_1)

        with pytest.raises(PhotoCollectionManager.CollectionError):
            PhotoCollectionManager.find_collection(photo_collection_settings_2)

    def test_find_collection_in_same_directory(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "photocollection")
        photo_collection_settings_file_path = pytest.utils.get_test_resources_path("photos", "photocollection", PhotoCollectionSettings.FILENAME)
        path = PhotoCollectionManager.find_collection(photo_collection_settings_path)
        assert path == os.path.dirname(photo_collection_settings_file_path)

    def test_find_collection_in_any_parent_directory(self):
        photo_collection_settings_path_1 = pytest.utils.get_test_resources_path("photos", "photocollection", "d1")
        photo_collection_settings_path_2 = pytest.utils.get_test_resources_path("photos", "photocollection", "d1", "d1_3", "d1_3_2")
        photo_collection_settings_file_path = pytest.utils.get_test_resources_path("photos", "photocollection", PhotoCollectionSettings.FILENAME)
        path = PhotoCollectionManager.find_collection(photo_collection_settings_path_1)
        assert path == os.path.dirname(photo_collection_settings_file_path)

        path = PhotoCollectionManager.find_collection(photo_collection_settings_path_2)
        assert path == os.path.dirname(photo_collection_settings_file_path)

    def test_find_collection_returns_None_without_settings_file(self):
        photo_collection_settings_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        path = PhotoCollectionManager.find_collection(photo_collection_settings_path)
        assert path == None

    def test_photo_index_number_of_images(self):
        super().base_test_photo_index_number_of_images(16)

    def test_photo_index_loads_all_photos_without_deleted_photos_folder_set(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.collection_settings.deleted_photos_folder = ""
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        assert len(photo_collection.photo_index) == 17

    def test_photo_index_loads_all_photos_with_deleted_photos_folder_set_to_non_existent_folder(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.collection_settings.deleted_photos_folder = "trash"
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        assert len(photo_collection.photo_index) == 17

    def test_get_list_of_all_tags(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        all_tags = photo_collection.get_list_of_all_tags()

        expected_tags = [
            "car",
            "building",
            ":2023",
            ":2023:random-images",
            ":2023:more-random-images",
            "pen",
            "paper",
            "ink",
            "pencil",
            "writing",
        ]

        assert sorted(all_tags) == sorted(expected_tags)

    def test_get_next_non_existing_path(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        directory_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023")
        photo_path_base = "2023-10-13-19-32-50-307-40x30"
        photo_ext = ".jpg"
        photo_path = os.path.join(directory_path, f"{photo_path_base}{photo_ext}")

        new_path = PhotoCollectionManager._get_next_non_existing_path(photo_path)
        assert new_path == os.path.join(directory_path, f"{photo_path_base}--1{photo_ext}")

        new_path = PhotoCollectionManager._get_next_non_existing_path(photo_path)
        assert new_path == os.path.join(directory_path, f"{photo_path_base}--1{photo_ext}")

        open(new_path, "w").write("hello world")

        new_path = PhotoCollectionManager._get_next_non_existing_path(photo_path)
        assert new_path == os.path.join(directory_path, f"{photo_path_base}--2{photo_ext}")

    def test_get_main_tag_photo_path(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        photo_1_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_2_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-50-307-40x30.jpg")

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None

        photo_1 = next(filter(lambda p: p.path == photo_1_path, photo_collection.photo_index), None)
        assert photo_1 is not None
        assert photo_collection._get_main_tag_photo_path(photo_1) == photo_1_path

        photo_2 = next(filter(lambda p: p.path == photo_2_path, photo_collection.photo_index), None)
        assert photo_2 is not None
        assert photo_collection._get_main_tag_photo_path(photo_2) == photo_2_path

    def test_cleanup_and_migrate_meta_data_and_directories(self):
        meta_file_path = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file")
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        assert photo_hash in MetaStorage(meta_file_path)._data
        assert "hash1" in MetaStorage(meta_file_path)._data
        assert "hash2" in MetaStorage(meta_file_path)._data
        assert len(MetaStorage(self.PHOTO_COLLECTION_PATH)._data) == 0
        meta_storage_object_hash1 = copy.deepcopy(MetaStorage(meta_file_path)._data["hash1"])
        meta_storage_object_hash2 = copy.deepcopy(MetaStorage(meta_file_path)._data["hash2"])

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        photo_collection.meta_storage_manager.cleanup_and_migrate_meta_data_and_directories(photo_collection.photo_index)

        assert photo_hash in MetaStorage(meta_file_path)._data
        assert "hash1" not in MetaStorage(meta_file_path)._data
        assert "hash2" not in MetaStorage(meta_file_path)._data
        assert len(MetaStorage(self.PHOTO_COLLECTION_PATH)._data) == 2

        assert "hash1" in MetaStorage(self.PHOTO_COLLECTION_PATH)._data
        assert "hash2" in MetaStorage(self.PHOTO_COLLECTION_PATH)._data
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash1"].filename == meta_storage_object_hash1.filename
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash1"].meta == meta_storage_object_hash1.meta
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash2"].filename == meta_storage_object_hash2.filename
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash2"].meta == meta_storage_object_hash2.meta

    def test_delete_photo_moves_photo_into_deleted_photos_folder(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        deleted_photos_folder = "garbage" # defined in collection.config (photocollection)
        photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-17-102-40x30.jpg")
        photo_path_deleted = pytest.utils.get_test_resources_path("photos", "photocollection", deleted_photos_folder, "2023-10-13-19-32-17-102-40x30.jpg")
        photo_hash = "eacd7aa32d3937909ddbe81864e690e4340096b5c25e585ecb711e0ffacc2bec"

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None

        photo_to_delete = next(filter(lambda p: p.path == photo_path, photo_collection.photo_index), None)
        assert photo_to_delete is not None

        assert photo_to_delete in photo_collection.photo_index
        assert os.path.exists(photo_path)
        assert not os.path.exists(photo_path_deleted)

        assert photo_hash in MetaStorage(photo_path)._data
        old_meta_storage_object = copy.deepcopy(MetaStorage(photo_path)._data[photo_hash])

        # delete photo
        photo_collection.delete_photo(photo_to_delete)

        assert photo_to_delete not in photo_collection.photo_index
        assert not os.path.exists(photo_path)
        assert os.path.exists(photo_path_deleted)

        # meta storage data is moved to the 'trash' directory
        assert photo_hash in MetaStorage(photo_path_deleted)._data
        new_meta_storage_object = copy.deepcopy(MetaStorage(photo_path_deleted)._data[photo_hash])
        assert new_meta_storage_object.filename == old_meta_storage_object.filename
        assert new_meta_storage_object.meta == old_meta_storage_object.meta

    def test_delete_photo_deletes_photo_from_system(self):
        # This tests check the setting deleted_photos_folder="" to not use a 'trash' directory and delete photos immediately from system
        os.remove = pytest.utils.FunctionSniffer(os.remove)
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-17-102-40x30.jpg")
        photo_hash = "eacd7aa32d3937909ddbe81864e690e4340096b5c25e585ecb711e0ffacc2bec"

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        # change setting to not use a 'trash' directory and delete photos immediately
        photo_collection.collection_settings.deleted_photos_folder = ""
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None

        photo_to_delete = next(filter(lambda p: p.path == photo_path, photo_collection.photo_index), None)
        assert photo_to_delete is not None

        assert photo_to_delete in photo_collection.photo_index
        assert os.path.exists(photo_path)

        assert photo_hash in MetaStorage(photo_path)._data
        old_meta_storage_object = copy.deepcopy(MetaStorage(photo_path)._data[photo_hash])

        # delete photo
        photo_collection.delete_photo(photo_to_delete)

        assert os.remove.calls == [((photo_to_delete.get_path(),), {})]

        assert photo_to_delete not in photo_collection.photo_index
        assert not os.path.exists(photo_path)

        # meta storage data is kept - future improvement is to delete 'orphaned' meta data after some grace period automatically
        assert photo_hash in MetaStorage(os.path.dirname(photo_path))._data
        new_meta_storage_object = copy.deepcopy(MetaStorage(os.path.dirname(photo_path))._data[photo_hash])
        assert new_meta_storage_object.filename == old_meta_storage_object.filename
        assert new_meta_storage_object.meta == old_meta_storage_object.meta

    def test_delete_duplicate_photos(self):
        photo_index = PhotoIndex(PhotoImplementation)
        photo_index.load(self.PHOTO_COLLECTION_PATH)
        all_hashes = list(photo.hash for photo in photo_index)
        all_paths = list(photo.path for photo in photo_index)
        unique_hashes = set(all_hashes)

        assert len(unique_hashes) < len(all_hashes)

        new_photo_index = PhotoCollectionManager.delete_duplicate_photos(photo_index)

        assert len(photo_index) > len(new_photo_index)
        assert new_photo_index.root_path == photo_index.root_path
        assert new_photo_index._photo_delegate == photo_index._photo_delegate

        new_all_hashes = list(photo.hash for photo in new_photo_index)
        new_all_paths = list(photo.path for photo in new_photo_index)
        new_unique_hashes = set(new_all_hashes)
        assert all(p in all_paths for p in new_all_paths)
        assert not all(p in new_all_paths for p in all_paths)
        assert new_unique_hashes == unique_hashes

    def test_autosort_photos_with_photo_index_within_collection(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        correct_directory = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images")
        wrong_directory = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "wrong-directory")
        photo_1_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_2_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_3_path_orig = pytest.utils.get_test_resources_path("photos", "photocollection", "d2_gps", "2023-10-13-19-32-46-670-40x30.jpg")
        photo_3_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "photo3.jpg")
        photo_4_path = pytest.utils.get_test_resources_path("photos", "photocollection", "d2_gps", "2023-10-13-19-33-03-352.jpg")

        # Move some photos with their meta storage files into 'wrong' locations as if someone messed up the autosorted photos.
        shutil.move(correct_directory, wrong_directory)
        shutil.move(photo_3_path_orig, photo_3_path)

        assert os.path.exists(photo_1_path)
        assert not os.path.exists(photo_2_path)
        assert os.path.exists(photo_3_path)
        assert os.path.exists(photo_4_path)

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        photo_collection.autosort_photos(photo_collection.photo_index)

        assert os.path.exists(photo_1_path), "photo should not have moved"
        assert os.path.exists(photo_2_path), "phoot should have moved"
        assert os.path.exists(photo_3_path), "photo should not have moved"
        assert os.path.exists(photo_4_path), "photo should not have moved"

    def test_autosort_photos_with_photo_index_outside_collection(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        photo_index_root = pytest.utils.get_test_resources_path("photos", "random-photo-index")
        collection_root_path = pytest.utils.get_test_resources_path("photos", "photocollection")
        collection_photo1_photo2_directory = os.path.join(collection_root_path, tagged_photos_root_folder, "2023", "random-images")
        photo_index_photo1_photo2_directory = os.path.join(photo_index_root, "random-subdir")
        photo_1_path = os.path.join(collection_root_path, tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_2_path = os.path.join(collection_root_path, tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_3_path_orig = os.path.join(collection_root_path, "d2_gps", "2023-10-13-19-32-46-670-40x30.jpg")
        photo_3_path_photo_index = pytest.utils.get_test_resources_path("photos", "random-photo-index", "random-subdir2", "photo3.jpg")
        photo_4_path = os.path.join(collection_root_path, "d2_gps", "2023-10-13-19-33-03-352.jpg")

        def get_photo_3_path_collection():
            added_dirs = [d for d in os.listdir(pytest.utils.get_test_resources_path("photos", "photocollection")) if d.startswith("added-at")]
            assert len(added_dirs) in (0, 1)
            if len(added_dirs) == 0:
                return None
            return os.path.join(collection_root_path, added_dirs[0], "random-subdir2", "photo3.jpg")

        # Move some photos with their meta storage files into 'wrong' locations as if someone messed up the autosorted photos.
        os.makedirs(photo_index_root)
        os.makedirs(os.path.dirname(photo_3_path_photo_index))
        shutil.move(collection_photo1_photo2_directory, photo_index_photo1_photo2_directory)
        shutil.move(photo_3_path_orig, photo_3_path_photo_index)

        assert os.path.exists(photo_1_path)
        assert not os.path.exists(photo_2_path)
        assert get_photo_3_path_collection() is None
        assert os.path.exists(photo_4_path)

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(photo_index_root)
        photo_collection.autosort_photos(photo_collection.photo_index)

        assert os.path.exists(photo_1_path), "photo should not have moved"
        assert os.path.exists(photo_2_path), "photo should have moved"
        assert os.path.exists(get_photo_3_path_collection()), "photo should have moved"
        assert os.path.exists(photo_4_path), "photo should not have moved"

    def test_autosort_photos_with_similar_but_not_the_same_path(self):
        pytest.utils.delete_all_license_files_from_resources() 
        photo_index_orig_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_index_root_path = pytest.utils.get_test_resources_path("photos", "photocollection-similar-directory")
        collection_root_path = pytest.utils.get_test_resources_path("photos", "photocollection")

        # Move some photos with their meta storage files into 'wrong' locations as if someone messed up the autosorted photos.
        shutil.move(photo_index_orig_path, photo_index_root_path)

        assert not os.path.exists(photo_index_orig_path)
        assert os.path.exists(photo_index_root_path)
        assert os.path.exists(collection_root_path)

        def get_added_photos_path():
            added_dirs = [d for d in os.listdir(pytest.utils.get_test_resources_path("photos", "photocollection")) if d.startswith("added-at")]
            assert len(added_dirs) == 1
            return os.path.join(collection_root_path, added_dirs[0])

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(collection_root_path)
        photo_collection.open_photos(photo_index_root_path)
        photo_collection.autosort_photos(photo_collection.photo_index)

        def assert_all_sorted10_photos():
            added_photos_path = get_added_photos_path()
            assert not os.path.exists(photo_index_root_path), "the directory of the photo index before move should be empty now and hence, removed"
            assert os.path.exists(added_photos_path), "a new directory for photos without main tags should have been created"
            assert os.path.exists(os.path.join(added_photos_path, "d1/2023-10-13-19-32-06-345-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d1/2023-10-13-19-32-11-778-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d1/d1_3/2023-10-13-19-32-17-102-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d1/d1_3/2023-10-13-19-32-36-602-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d1/d1_3/2023-10-13-19-32-40-737-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d1/d1_3/d1_3_2/2023-10-13-19-32-25-376-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d1/d1_3/d1_3_2/2023-10-13-19-32-31-105-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d2_gps/2023-10-13-19-32-46-670-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d2_gps/2023-10-13-19-32-50-307-40x30.jpg")), "photo should be moved here"
            assert os.path.exists(os.path.join(added_photos_path, "d2_gps/2023-10-13-19-33-03-352.jpg")), "photo should be moved here"
        assert_all_sorted10_photos()

        # this should be a nop now because the photo_index_root_path is inside the collection_root_path
        photo_collection.autosort_photos(photo_collection.photo_index)
        assert_all_sorted10_photos()

    def test_cleanup(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        photo_1_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_2_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_3_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "random-images", "2023-10-13-19-32-17-102-40x30.jpg")
        some_meta_file_directory = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file")
        photo_4_new_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "more-random-images", "2023-10-13-19-32-25-376-40x30.jpg")
        photo_4_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        assert os.path.exists(photo_1_path)
        assert os.path.exists(photo_2_path)

        assert photo_4_hash in MetaStorage(some_meta_file_directory)._data
        assert "hash1" in MetaStorage(some_meta_file_directory)._data
        assert "hash2" in MetaStorage(some_meta_file_directory)._data
        assert len(MetaStorage(self.PHOTO_COLLECTION_PATH)._data) == 0
        meta_storage_object_hash1 = copy.deepcopy(MetaStorage(some_meta_file_directory)._data["hash1"])
        meta_storage_object_hash2 = copy.deepcopy(MetaStorage(some_meta_file_directory)._data["hash2"])

        # Move some photos without meta storage files into 'wrong' locations as if someone messed up the photo locoations.
        wrong_directory = os.path.join(self.PHOTO_COLLECTION_PATH, "autogenerated-wrong-directory")
        os.mkdir(wrong_directory)
        shutil.move(photo_1_path, wrong_directory)
        shutil.move(photo_2_path, some_meta_file_directory)
        shutil.move(photo_3_path, self.PHOTO_COLLECTION_PATH)

        assert not os.path.exists(photo_1_path)
        assert not os.path.exists(photo_2_path)
        assert not os.path.exists(photo_3_path)

        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        photo_collection.cleanup_and_integrate()

        assert os.path.exists(photo_1_path)
        assert not os.path.exists(photo_2_path) # duplicate of photo_1
        assert os.path.exists(photo_3_path)

        assert photo_4_hash in MetaStorage(photo_4_new_path)._data
        if os.path.exists(some_meta_file_directory):
            assert photo_4_hash not in MetaStorage(some_meta_file_directory)._data
            assert "hash1" not in MetaStorage(some_meta_file_directory)._data
            assert "hash2" not in MetaStorage(some_meta_file_directory)._data
        assert len(MetaStorage(self.PHOTO_COLLECTION_PATH)._data) == 2

        assert "hash1" in MetaStorage(self.PHOTO_COLLECTION_PATH)._data
        assert "hash2" in MetaStorage(self.PHOTO_COLLECTION_PATH)._data
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash1"].filename == meta_storage_object_hash1.filename
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash1"].meta == meta_storage_object_hash1.meta
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash2"].filename == meta_storage_object_hash2.filename
        assert MetaStorage(self.PHOTO_COLLECTION_PATH)._data["hash2"].meta == meta_storage_object_hash2.meta

    def test_move_file_with_set_path_moves_meta_storage_data(self, file_access_patcher):
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", "d3", "moved-photo.jpg")
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        assert photo_hash in MetaStorage(old_photo_path)._data
        old_meta_storage_object_photo = copy.deepcopy(MetaStorage(old_photo_path)._data[photo_hash])

        super().base_test_move_file_with_set_path(old_photo_path, new_photo_path)
        # moving photo moves meta data realted to photo into the new meta_storage
        assert photo_hash not in MetaStorage(os.path.dirname(old_photo_path))._data
        assert photo_hash in MetaStorage(new_photo_path)._data
        assert MetaStorage(new_photo_path)._data[photo_hash].filename == old_meta_storage_object_photo.filename
        assert MetaStorage(new_photo_path)._data[photo_hash].meta == old_meta_storage_object_photo.meta

    def test_move_file_with_set_path_deletes_directories_if_no_meta_data_left_in_old_storage(self):
        pytest.utils.delete_all_license_files_from_resources() 
        old_photo_path_1 = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        old_photo_path_2 = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file", "2023-10-13-19-32-31-105-40x30.jpg")
        old_photo_path_3 = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file", "2023-10-13-19-32-40-737-40x30.jpg")
        new_photo_path_1 = pytest.utils.get_test_resources_path("photos", "photocollection", "d3", "moved-photo-1.jpg")
        new_photo_path_2 = pytest.utils.get_test_resources_path("photos", "photocollection", "d3", "moved-photo-2.jpg")
        new_photo_path_3 = pytest.utils.get_test_resources_path("photos", "photocollection", "d3", "moved-photo-3.jpg")
        photo_1_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        # move a file which is not registered in meta_storage
        super().base_test_move_file_with_set_path(old_photo_path_2, new_photo_path_2)
        assert not os.path.exists(old_photo_path_2)
        # meta data for photo_1 is still in meta storage
        assert photo_1_hash in MetaStorage(old_photo_path_1)._data
        old_meta_storage_object_photo = copy.deepcopy(MetaStorage(old_photo_path_1)._data[photo_1_hash])

        # move a file which is registered in meta_storage
        super().base_test_move_file_with_set_path(old_photo_path_1, new_photo_path_1)
        assert not os.path.exists(old_photo_path_1)
        # photo_1 moved, so does meta data move into another meta storage
        assert photo_1_hash not in MetaStorage(os.path.dirname(old_photo_path_1))._data
        assert photo_1_hash in MetaStorage(new_photo_path_1)._data
        assert MetaStorage(new_photo_path_1)._data[photo_1_hash].filename == old_meta_storage_object_photo.filename
        assert MetaStorage(new_photo_path_1)._data[photo_1_hash].meta == old_meta_storage_object_photo.meta

        # move a file which is not registered in meta_storage
        # no photo remaining in this directory, but 'orphaned' meta data is still present
        # in old directory
        super().base_test_move_file_with_set_path(old_photo_path_3, new_photo_path_3)
        assert not os.path.exists(old_photo_path_3)
        assert os.path.exists(os.path.dirname(old_photo_path_3))

        # delete 'orphaned' meta data still present in the directory without any images
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        photo_collection.cleanup_and_integrate()

        assert not os.path.exists(os.path.dirname(old_photo_path_3))

    def test_set_main_tag_moves_meta_storage_data(self):
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        main_tag = ":2023:germany:aachen"
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, *main_tag.lstrip(":").split(":"), os.path.basename(old_photo_path))
        photo_hash = "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777"

        assert photo_hash in MetaStorage(old_photo_path)._data
        old_meta_storage_object = copy.deepcopy(MetaStorage(old_photo_path)._data[photo_hash])
        assert not os.path.exists(new_photo_path)
        
        super().base_test_set_main_tag(old_photo_path, main_tag)

        assert photo_hash in MetaStorage(new_photo_path)._data
        assert MetaStorage(new_photo_path)._data[photo_hash].filename == old_meta_storage_object.filename
        for key in old_meta_storage_object.meta:
            assert MetaStorage(new_photo_path)._data[photo_hash].meta[key] == old_meta_storage_object.meta[key]
        assert MetaStorage(new_photo_path)._data[photo_hash].meta["tags"] == [main_tag]

    def test_change_main_tag_moves_file_into_subdir(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        main_tag = ":2023:aachen"
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, *main_tag.lstrip(":").split(":"), "2023-10-13-19-32-50-307-40x30.jpg")

        assert os.path.exists(old_photo_path)
        assert not os.path.exists(new_photo_path)

        super().base_test_set_main_tag(old_photo_path, main_tag)
        assert not os.path.exists(old_photo_path)
        assert os.path.exists(new_photo_path)

    def test_change_main_tag_moves_file_into_sibling_dir(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        main_tag = ":2022"
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, *main_tag.lstrip(":").split(":"), "2023-10-13-19-32-50-307-40x30.jpg")

        assert os.path.exists(old_photo_path)
        assert not os.path.exists(new_photo_path)

        super().base_test_set_main_tag(old_photo_path, main_tag)
        assert not os.path.exists(old_photo_path)
        assert os.path.exists(new_photo_path)

    def test_change_main_tag_moves_meta_storage_data(self):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        main_tag = ":2023:aachen"
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, *main_tag.lstrip(":").split(":"), "2023-10-13-19-32-50-307-40x30.jpg")
        photo_hash = "fb432dbb1fbe2d333b6d0854199693b0bb391b0ee4348565f6b7052790524533"

        assert photo_hash in MetaStorage(old_photo_path)._data
        old_meta_storage_object = copy.deepcopy(MetaStorage(old_photo_path)._data[photo_hash])

        super().base_test_set_main_tag(old_photo_path, main_tag)

        assert photo_hash in MetaStorage(new_photo_path)._data
        assert MetaStorage(new_photo_path)._data[photo_hash].filename == old_meta_storage_object.filename
        assert MetaStorage(new_photo_path)._data[photo_hash].meta != old_meta_storage_object.meta
        assert sorted(MetaStorage(new_photo_path)._data[photo_hash].meta["tags"]) == [main_tag, "building", "car"]
        del old_meta_storage_object.meta["tags"]
        del MetaStorage(new_photo_path)._data[photo_hash].meta["tags"]
        assert MetaStorage(new_photo_path)._data[photo_hash].meta == old_meta_storage_object.meta

    def test_remove_main_tag_moves_file(self, file_access_patcher):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", "2023-10-13-19-32-50-307-40x30.jpg")

        assert os.path.exists(old_photo_path)
        assert not os.path.exists(new_photo_path)

        # load photo collection, get photo object, and delete main tag
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        photo = next(filter(lambda p: p.path == old_photo_path, photo_collection.photo_index), None)
        assert photo is not None
        photo.set_main_tag(None)
        assert photo.main_tag is None

        assert photo.path == new_photo_path

        assert not os.path.exists(old_photo_path)
        assert os.path.exists(new_photo_path)

    def test_remove_main_tag_moves_meta_storage_data(self, file_access_patcher):
        tagged_photos_root_folder = "autosorted" # defined in collection.config (photocollection)
        old_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", tagged_photos_root_folder, "2023", "2023-10-13-19-32-50-307-40x30.jpg")
        photo_hash = "fb432dbb1fbe2d333b6d0854199693b0bb391b0ee4348565f6b7052790524533"
        new_photo_path = pytest.utils.get_test_resources_path("photos", "photocollection", "2023-10-13-19-32-50-307-40x30.jpg")

        assert photo_hash in MetaStorage(old_photo_path)._data
        old_meta_storage_object = copy.deepcopy(MetaStorage(old_photo_path)._data[photo_hash])

        # load photo collection, get photo object, and delete main tag
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        photo_collection.open_photos(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.photo_index is not None
        photo = next(filter(lambda p: p.path == old_photo_path, photo_collection.photo_index), None)
        assert photo is not None
        photo.main_tag = None
        assert photo.main_tag is None

        assert photo_hash in MetaStorage(new_photo_path)._data
        assert MetaStorage(new_photo_path)._data[photo_hash].filename == old_meta_storage_object.filename
        assert MetaStorage(new_photo_path)._data[photo_hash].meta != old_meta_storage_object.meta
        assert sorted(MetaStorage(new_photo_path)._data[photo_hash].meta["tags"]) == ["building", "car"]
        del old_meta_storage_object.meta["tags"]
        del MetaStorage(new_photo_path)._data[photo_hash].meta["tags"]
        assert MetaStorage(new_photo_path)._data[photo_hash].meta == old_meta_storage_object.meta


class TestPhotoCollectionManagerPreExistingSubdirectory(PhotoCollectionManagerTestBase):
    PHOTO_COLLECTION_PATH = pytest.utils.get_test_resources_path("photos", "photocollection", "d1")

    #override
    def test_set_collection_root(self):
        photo_collection = PhotoCollectionManager(PhotoImplementation)
        assert photo_collection.collection_root is None
        assert photo_collection.meta_storage_manager is None
        photo_collection.set_collection_root(self.PHOTO_COLLECTION_PATH)
        assert photo_collection.collection_root == os.path.dirname(self.PHOTO_COLLECTION_PATH)
        assert isinstance(photo_collection.meta_storage_manager, PhotoCollectionMetaStorageManager)

    def test_create_fails_within_collection(self):
        collection_root = PhotoCollectionManager.find_collection(self.PHOTO_COLLECTION_PATH)
        assert self.PHOTO_COLLECTION_PATH.startswith(collection_root)
        with pytest.raises(AssertionError):
            PhotoCollectionManager.create(self.PHOTO_COLLECTION_PATH)

    def test_photo_index_number_of_images(self):
        super().base_test_photo_index_number_of_images(7)
