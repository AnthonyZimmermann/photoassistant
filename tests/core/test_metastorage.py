# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import builtins
import datetime
import logging
import os
import pytest
import time

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

from photoassistant.core.metastorage import MetaStorage
from photoassistant.core.metastorage import MetaStorageObject
from photoassistant.core.metastorage import MetaStorageVersionUtil


class TestMetaStorageObject:
    def test_instantiation_fails_without_filename(self):
        with pytest.raises(TypeError):
            MetaStorageObject()
        with pytest.raises(TypeError):
            MetaStorageObject(meta=dict(key_1="value_1"))

    def test_instantiation_works_with_filename(self):
        MetaStorageObject(filename="filename")
        MetaStorageObject(filename="filename", meta=dict(key_1="value_1"))
        MetaStorageObject(filename="filename", meta=dict(key_1="value_1"), update_timestamp=datetime.datetime.now(datetime.timezone.utc))
        MetaStorageObject(filename="filename", update_timestamp=datetime.datetime.now(datetime.timezone.utc))

    def test_creation_without_meta_without_update_timestamp(self):
        meta_storage_object = MetaStorageObject(filename="filename")
        assert meta_storage_object.filename == "filename"
        assert meta_storage_object.meta == dict()
        assert isinstance(meta_storage_object.update_timestamp, datetime.datetime)
        assert meta_storage_object.update_timestamp <= datetime.datetime.now(datetime.timezone.utc)
        assert meta_storage_object.update_timestamp > datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=1)

    def test_creation_without_meta(self):
        meta_storage_object = MetaStorageObject(filename="filename", update_timestamp=datetime.datetime(2000, 1, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc))
        assert meta_storage_object.filename == "filename"
        assert meta_storage_object.meta == dict()
        assert meta_storage_object.update_timestamp == datetime.datetime(2000, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc)

    def test_creation_without_update_timestamp(self):
        meta_storage_object = MetaStorageObject(filename="filename", meta=dict(key_1="value_1"))
        assert meta_storage_object.filename == "filename"
        assert meta_storage_object.meta == dict(key_1="value_1")
        assert isinstance(meta_storage_object.update_timestamp, datetime.datetime)
        assert meta_storage_object.update_timestamp <= datetime.datetime.now(datetime.timezone.utc)
        assert meta_storage_object.update_timestamp > datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=1)

    def test_serialization(self):
        meta_storage_object = MetaStorageObject(filename="image0123.jpg", update_timestamp=datetime.datetime(2000, 1, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc))
        serialized = meta_storage_object.serialize()
        assert serialized == {'filename': 'image0123.jpg', 'update_timestamp': 946688461000, 'meta': {}}

        meta_storage_object = MetaStorageObject(filename="image0123.jpg", meta=dict(a=1, b="2", c=[3], d=dict(x="y")))
        meta_storage_object._set_update_timestamp(datetime.datetime(2000, 1, 1, 1, 1, 1, 1, tzinfo=datetime.timezone.utc))
        serialized = meta_storage_object.serialize()
        assert serialized == {'filename': 'image0123.jpg', 'update_timestamp': 946688461000, 'meta': {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}}

    def test_load_serialized(self):
        serialized_meta = {'version': '1.0', 'filename': 'image0123.jpg', 'update_timestamp': 946688461000, 'meta': {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}}
        meta_storage_object = MetaStorageObject.load_serialized(serialized_meta)
        assert meta_storage_object.filename == "image0123.jpg"
        assert meta_storage_object.update_timestamp == datetime.datetime(2000, 1, 1, 1, 1, 1, 0, tzinfo=datetime.timezone.utc)
        assert meta_storage_object.meta == {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}

    def test_load_serialized_fails_with_wrong_version(self):
        serialized_meta = {'version': '-1.0', 'filename': 'image0123.jpg', 'update_timestamp': '2000-01-01T01:01:01.000001+00:00', 'meta': {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}}
        with pytest.raises(MetaStorage.LoadException):
            MetaStorageObject.load_serialized(serialized_meta)

        serialized_meta = {'version': 'xyz', 'filename': 'image0123.jpg', 'update_timestamp': '2000-01-01T01:01:01.000001+00:00', 'meta': {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}}
        with pytest.raises(MetaStorage.LoadException):
            MetaStorageObject.load_serialized(serialized_meta)

    def test_load_serialized_fails_without_filename(self):
        serialized_meta = {'version': '1.0', 'update_timestamp': '2000-01-01T01:01:01.000001+00:00', 'meta': {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}}
        with pytest.raises(MetaStorage.LoadException):
            MetaStorageObject.load_serialized(serialized_meta)

    def test_load_serialized_fails_without_update_timestamp(self):
        serialized_meta = {'version': '1.0', 'filename': 'image0123.jpg', 'meta': {'a': 1, 'b': '2', 'c': [3], 'd': {'x': 'y'}}}
        with pytest.raises(MetaStorage.LoadException):
            MetaStorageObject.load_serialized(serialized_meta)

    def test_load_serialized_fails_without_meta(self):
        serialized_meta = {'version': '1.0', 'filename': 'image0123.jpg', 'update_timestamp': '2000-01-01T01:01:01.000001+00:00'}
        with pytest.raises(MetaStorage.LoadException):
            MetaStorageObject.load_serialized(serialized_meta)

    def test_meta_storage_serialized_can_be_loaded(self):
        meta_storage_object_1 = MetaStorageObject(filename="image0123.jpg")
        meta_storage_object_2 = MetaStorageObject.load_serialized(meta_storage_object_1.serialize())
        assert meta_storage_object_1.filename == meta_storage_object_2.filename
        assert meta_storage_object_1.update_timestamp == meta_storage_object_2.update_timestamp
        assert meta_storage_object_1.meta == meta_storage_object_2.meta


class TestMetaStorageVersionUtil:
    def test_left_version_less_equal_returns_true_if_left_is_less(self):
        assert MetaStorageVersionUtil.left_version_less_equal("1.0", "2.0") == True
        assert MetaStorageVersionUtil.left_version_less_equal("1.0000", "2.0") == True
        assert MetaStorageVersionUtil.left_version_less_equal("1.0", "3.0") == True
        assert MetaStorageVersionUtil.left_version_less_equal("2.0", "10.0") == True
        assert MetaStorageVersionUtil.left_version_less_equal("1.0", "1.1") == True
        assert MetaStorageVersionUtil.left_version_less_equal("2.1", "10.1") == True
        assert MetaStorageVersionUtil.left_version_less_equal("2.10", "10.1") == True

    def test_left_version_less_equal_returns_true_if_left_is_equal(self):
        for value in ("1.0", "1.1", "1.3", "1.11", "21.11", "1.800", "81.21"):
            assert MetaStorageVersionUtil.left_version_less_equal(value, value) == True

    def test_left_version_less_equal_returns_false_if_left_is_greater(self):
        assert MetaStorageVersionUtil.left_version_less_equal("2.0", "1.0") == False
        assert MetaStorageVersionUtil.left_version_less_equal("2.0000", "1.0") == False
        assert MetaStorageVersionUtil.left_version_less_equal("3.0", "1.0") == False
        assert MetaStorageVersionUtil.left_version_less_equal("10.0", "2.0") == False
        assert MetaStorageVersionUtil.left_version_less_equal("1.2", "1.1") == False
        assert MetaStorageVersionUtil.left_version_less_equal("10.1", "2.9") == False
        assert MetaStorageVersionUtil.left_version_less_equal("10.1", "2.11") == False


class TestMetaStorageInstantiation:
    def test_instantiation(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")
        MetaStorage(search_path)

    def test_instantiation_with_filepath(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file", "2023-10-13-19-32-06-345-40x30.jpg")
        MetaStorage(photo_path)

    def test_instantiation_on_same_path_returns_same_object(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file", "2023-10-13-19-32-06-345-40x30.jpg")
        obj_1 = MetaStorage(search_path)
        obj_2 = MetaStorage(search_path)
        obj_3 = MetaStorage(photo_path)
        assert obj_1 == obj_2 == obj_3

    def test_instantiation_on_different_paths_returns_different_objects(self):
        search_path_1 = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files")
        search_path_2 = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "empty_meta_file", "2023-10-13-19-32-17-102-40x30.jpg")
        assert search_path_1 != search_path_2
        assert search_path_1 != photo_path
        assert search_path_2 != photo_path
        obj_1 = MetaStorage(search_path_1)
        obj_2 = MetaStorage(search_path_2)
        obj_3 = MetaStorage(photo_path)
        assert obj_1 != obj_2
        assert obj_1 != obj_2
        assert obj_2 != obj_3


class TestMetaStorageLoading:
    def test_meta_storage_error_if_path_is_not_present(self):
        with pytest.raises(MetaStorage.LoadException):
            MetaStorage("path_is_not_present")

        # As the path is not present at all, the instantiation will never work opposed to
        # e.g., corrupted meta files where the load_exception variable keeps the exceptions
        # that ocurred on inital loading, but the MetaStorage still works!
        with pytest.raises(MetaStorage.LoadException):
            MetaStorage("path_is_not_present")

    def test_meta_storage_loads_without_meta_file(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")
        meta_storage = MetaStorage(search_path)
        assert meta_storage._meta_path == None
        assert meta_storage._data == dict()
        assert meta_storage.load_exception is None

    def test_meta_storage_loads_empty_meta_file(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "empty_meta_file")
        meta_storage = MetaStorage(search_path)
        assert meta_storage._meta_path == os.path.abspath(os.path.join(search_path, "random-filename.meta"))
        assert meta_storage._data == dict()
        assert meta_storage.load_exception is None

    def test_meta_storage_loads_meta_file_with_random_entries(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        assert meta_storage._meta_path == os.path.abspath(os.path.join(search_path, "deadbeef.meta"))
        assert meta_storage.load_exception is None

        assert "hash1" in meta_storage.get_hashes()
        meta_storage_object_hash1 = meta_storage._data["hash1"]
        assert meta_storage_object_hash1.filename == "2023-10-13-19-32-25-376-40x30.jpg"
        assert meta_storage_object_hash1.meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}
        
        assert "hash2" in meta_storage.get_hashes()
        meta_storage_object_hash2 = meta_storage._data["hash2"]
        assert meta_storage_object_hash2.filename == "2023-10-13-19-32-31-105-40x30.jpg"
        assert meta_storage_object_hash2.meta == {"creation_timestamp": 1672527600000, "image_orientation": 4}

    def test_meta_storage_loads_multiple_meta_files_and_creates_a_new_file(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_multiple_meta_files")
        assert all(os.path.exists(os.path.join(search_path, f)) for f in ("affe_1.meta", "affe_2.meta", "affe_3.meta", "affe_4.meta"))

        meta_storage = MetaStorage(search_path)
        assert not any(os.path.exists(os.path.join(search_path, f)) for f in ("affe_1.meta", "affe_2.meta", "affe_3.meta", "affe_4.meta"))
        assert meta_storage._meta_path is not None
        assert os.path.exists(meta_storage._meta_path)
        assert os.path.dirname(meta_storage._meta_path) == search_path
        assert os.path.basename(meta_storage._meta_path).endswith(meta_storage._meta_extension)

    def test_meta_storage_loads_multiple_meta_files_and_combines_data(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_multiple_meta_files")
        meta_storage = MetaStorage(search_path)
        assert meta_storage._meta_path is not None

        assert len(meta_storage) == 4
 
        assert "hash1" in meta_storage.get_hashes()
        meta_storage_object_hash1 = meta_storage._data["hash1"]
        assert meta_storage_object_hash1.filename == "2023-10-13-19-32-25-376-40x30.jpg"
        assert meta_storage_object_hash1.update_timestamp == datetime.datetime(2023, 10, 20, 15, 0, 0, 0, tzinfo=datetime.timezone.utc)
        assert meta_storage_object_hash1.meta == {"creation_timestamp": 1672614000000, "image_orientation": 8}
        
        assert "hash2" in meta_storage.get_hashes()
        meta_storage_object_hash2 = meta_storage._data["hash2"]
        assert meta_storage_object_hash2.filename == "2023-10-13-19-32-31-105-40x30.jpg"
        assert meta_storage_object_hash2.update_timestamp == datetime.datetime(2023, 3, 1, 15, 0, 0, 0, tzinfo=datetime.timezone.utc)
        assert meta_storage_object_hash2.meta == {"creation_timestamp": 1688162400000, "image_orientation": 6}

        assert "hash3" in meta_storage.get_hashes()
        meta_storage_object_hash3 = meta_storage._data["hash3"]
        assert meta_storage_object_hash3.filename == "2023-10-13-19-32-31-105-40x30.jpg"
        assert meta_storage_object_hash3.update_timestamp == datetime.datetime(2023, 10, 20, 16, 0, 0, 0, tzinfo=datetime.timezone.utc)
        assert meta_storage_object_hash3.meta == {"creation_timestamp": 1640991600000, "image_orientation": 1}

        assert "hash4" in meta_storage.get_hashes()
        meta_storage_object_hash4 = meta_storage._data["hash4"]
        assert meta_storage_object_hash4.filename == "2023-10-13-19-32-46-670-40x30.jpg"
        assert meta_storage_object_hash4.update_timestamp == datetime.datetime(2023, 10, 20, 15, 0, 0, 0, tzinfo=datetime.timezone.utc)
        assert meta_storage_object_hash4.meta == {"creation_timestamp": 1672527600000, "image_orientation": 6}
 

    def test_meta_storage_catches_exception_if_meta_file_header_is_broken(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "broken_meta_file", "broken_meta_header")
        assert MetaStorage(search_path).load_exception is not None
        assert isinstance(MetaStorage(search_path).load_exception, MetaStorage.LoadException)

    def test_meta_storage_catches_exception_if_meta_file_data_is_broken(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "broken_meta_file", "broken_meta_data", "missing_filename")
        assert MetaStorage(search_path).load_exception is not None
        assert isinstance(MetaStorage(search_path).load_exception, MetaStorage.LoadException)

        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "broken_meta_file", "broken_meta_data", "missing_meta")
        assert MetaStorage(search_path).load_exception is not None
        assert isinstance(MetaStorage(search_path).load_exception, MetaStorage.LoadException)

        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "broken_meta_file", "broken_meta_data", "missing_update_timestamp")
        assert MetaStorage(search_path).load_exception is not None
        assert isinstance(MetaStorage(search_path).load_exception, MetaStorage.LoadException)

    def test_meta_storage_loads_all_valid_meta_data_and_does_not_remove_corrupt_files(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "broken_meta_file", "partially_broken_meta")
        good_meta_file = os.path.join(search_path, "good.meta")
        broken_meta_files = [
            os.path.join(search_path, f) for f in
            ("corrupt.meta", "corrupt2.meta", "not-a-valid-meta-file.meta")
        ]

        assert os.path.exists(good_meta_file)
        for broken_meta_file in broken_meta_files:
            assert os.path.exists(broken_meta_file)

        assert isinstance(MetaStorage(search_path).load_exception, MetaStorage.LoadException)

        assert not os.path.exists(good_meta_file)
        for broken_meta_file in broken_meta_files:
            assert os.path.exists(broken_meta_file)

        expected_loaded_meta_data_hashes = [
            'corrupt2_hash1',
            'corrupt2_hash2',
            'corrupt2_hash3',
            'corrupt_66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777',
            'corrupt_hash1',
            'corrupt_hash4',
            'good_1aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777',
            'good_hash1',
            'good_hash2',
            'good_hash3',
            'good_hash4',
        ]
        assert sorted(MetaStorage(search_path).get_hashes()) == sorted(expected_loaded_meta_data_hashes)
        assert not os.path.exists(good_meta_file)
        for broken_meta_file in broken_meta_files:
            assert os.path.exists(broken_meta_file)

    def test_meta_storage_reuses_existing_meta_file(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        assert meta_storage._meta_path == os.path.abspath(os.path.join(search_path, "deadbeef.meta"))

class TestMetaStorage:
    def test_meta_storage_len(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        assert len(meta_storage) == 3

    def test_meta_storage_get_hashes(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)

        expected_hashes = [
            "df5621aa66af51ee31e5ecfbc680f37c651c76a7814fda40f1549b8bc0265777",
            "hash1",
            "hash2",
        ]
        assert sorted(meta_storage.get_hashes()) == sorted(expected_hashes)

    def test_meta_storage_get_meta_storage_object(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        meta_storage_object_hash1 = meta_storage._get_meta_storage_object("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert isinstance(meta_storage_object_hash1, MetaStorageObject)
        assert meta_storage_object_hash1.filename == "2023-10-13-19-32-25-376-40x30.jpg"
        assert meta_storage_object_hash1.meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}

    def test_meta_storage_get_meta_storage_object_filename_check_changes_filename(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        meta_storage_object_hash1 = meta_storage._get_meta_storage_object("hash1", "new-filename.jpg")
        assert isinstance(meta_storage_object_hash1, MetaStorageObject)
        assert meta_storage_object_hash1.filename == "new-filename.jpg"
        assert meta_storage_object_hash1.meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}

    def test_meta_storage_get_meta_storage_object_without_filename_check(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        meta_storage_object_hash1 = meta_storage._get_meta_storage_object("hash1", None)
        assert isinstance(meta_storage_object_hash1, MetaStorageObject)
        assert meta_storage_object_hash1.filename == "2023-10-13-19-32-25-376-40x30.jpg"
        assert meta_storage_object_hash1.meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}

    def test_meta_storage_delete_meta_storage_object(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        meta_storage_object_hash1 = meta_storage._get_meta_storage_object("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert isinstance(meta_storage_object_hash1, MetaStorageObject)
        assert meta_storage_object_hash1.filename == "2023-10-13-19-32-25-376-40x30.jpg"
        assert meta_storage_object_hash1.meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}

        meta_storage.delete_meta_storage_object("hash1")
        assert "hash1" not in meta_storage.get_hashes()
        meta_storage_object_hash1 = meta_storage._get_meta_storage_object("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert meta_storage_object_hash1.filename == "2023-10-13-19-32-25-376-40x30.jpg"
        assert meta_storage_object_hash1.meta == {}
    
    def test_meta_storage_get_meta(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        meta = meta_storage.get_meta("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}

    def test_meta_storage_get_meta_without_filename_check(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)
        meta = meta_storage.get_meta("hash1")
        assert meta == {"creation_timestamp": 1672527600000, "image_orientation": 3}
        assert meta_storage._get_meta_storage_object("hash1", None).filename == "2023-10-13-19-32-25-376-40x30.jpg"

    def test_meta_storage_set_meta(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        update_filename = "new_filename"
        update_meta = dict(image_orientation=8, new_key_1="value_1", new_key_2="value_2")
        dt_new = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=1)

        meta_storage = MetaStorage(search_path)
        meta_storage_object = meta_storage._get_meta_storage_object("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert meta_storage_object.meta != update_meta
        assert meta_storage_object.filename != update_filename
        assert meta_storage_object.update_timestamp < dt_new

        meta_storage.set_meta("hash1", update_filename, meta=update_meta)
        assert meta_storage._data["hash1"].meta == update_meta
        assert meta_storage._data["hash1"].filename == update_filename
        assert meta_storage._data["hash1"].update_timestamp >= dt_new

    def test_meta_storage_update_meta(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        update_filename = "new_filename"
        update_meta = dict(image_orientation=8, new_key_1="value_1", new_key_2="value_2")
        dt_new = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=1)

        meta_storage = MetaStorage(search_path)
        meta_storage_object = meta_storage._get_meta_storage_object("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert meta_storage_object.filename != update_filename
        assert meta_storage_object.update_timestamp < dt_new
        for update_key, update_value in update_meta.items():
            assert meta_storage_object.meta.get(update_key, None) != update_value

        meta_storage.update_meta("hash1", update_filename, meta=update_meta)
        assert meta_storage._data["hash1"].filename == update_filename
        assert meta_storage._data["hash1"].update_timestamp >= dt_new
        assert meta_storage._data["hash1"].meta == {
            "creation_timestamp": 1672527600000,
            "image_orientation": 8,
            "new_key_1": "value_1",
            "new_key_2": "value_2",
        }

    def test_meta_storage_delete_meta(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        update_filename = "new_filename"
        dt_new = datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(seconds=1)

        meta_storage = MetaStorage(search_path)
        meta_storage_object = meta_storage._get_meta_storage_object("hash1", "2023-10-13-19-32-25-376-40x30.jpg")
        assert meta_storage_object.filename != update_filename
        assert meta_storage_object.update_timestamp < dt_new
        assert "image_orientation" in meta_storage_object.meta

        meta_storage.delete_meta("hash1", update_filename, ["image_orientation"])
        assert meta_storage._data["hash1"].meta == {"creation_timestamp": 1672527600000}
        assert meta_storage._data["hash1"].filename == update_filename
        assert meta_storage._data["hash1"].update_timestamp >= dt_new

    def test_meta_storage_delete_meta_works_for_different_path_depths(self):
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")

        meta_storage = MetaStorage(search_path)

        dt_new = datetime.datetime.now(datetime.timezone.utc)
        dt_new = dt_new.replace(microsecond=int(dt_new.microsecond / 1000) * 1000)
        meta_storage.set_meta("hash0123", "filename0123", {"a": {"a.a": {"a.a.a": 1}, "a.b": {"a.b.a": 2, "a.b.b": None}}, "b": None, "c": 3, "d": {"d.a": None}})
        assert meta_storage._data["hash0123"].update_timestamp >= dt_new

        time.sleep(1e-6) # datetime in isoformat tracks microseconds
        dt_new = datetime.datetime.now(datetime.timezone.utc)
        dt_new = dt_new.replace(microsecond=int(dt_new.microsecond / 1000) * 1000)
        meta_storage.delete_meta("hash0123", "filename0123", ("a", "a.a", "a.a.a"))
        assert meta_storage._data["hash0123"].meta == {"a": {"a.b": {"a.b.a": 2, "a.b.b": None}}, "b": None, "c": 3, "d": {"d.a": None}}
        assert meta_storage._data["hash0123"].filename == "filename0123"
        assert meta_storage._data["hash0123"].update_timestamp >= dt_new

        time.sleep(1e-6) # datetime in isoformat tracks microseconds
        dt_new = datetime.datetime.now(datetime.timezone.utc)
        dt_new = dt_new.replace(microsecond=int(dt_new.microsecond / 1000) * 1000)
        meta_storage.delete_meta("hash0123", "filename0123", ("a", "a.b", "a.b.a"))
        assert meta_storage._data["hash0123"].meta == {"a": {"a.b": {"a.b.b": None}}, "b": None, "c": 3, "d": {"d.a": None}}
        assert meta_storage._data["hash0123"].filename == "filename0123"
        assert meta_storage._data["hash0123"].update_timestamp >= dt_new

        time.sleep(1e-6) # datetime in isoformat tracks microseconds
        dt_new = datetime.datetime.now(datetime.timezone.utc)
        dt_new = dt_new.replace(microsecond=int(dt_new.microsecond / 1000) * 1000)
        meta_storage.delete_meta("hash0123", "filename0123", ("a", "a.b"))
        assert meta_storage._data["hash0123"].meta == {"b": None, "c": 3, "d": {"d.a": None}}
        assert meta_storage._data["hash0123"].filename == "filename0123"
        assert meta_storage._data["hash0123"].update_timestamp >= dt_new

        time.sleep(1e-6) # datetime in isoformat tracks microseconds
        dt_new = datetime.datetime.now(datetime.timezone.utc)
        dt_new = dt_new.replace(microsecond=int(dt_new.microsecond / 1000) * 1000)
        meta_storage.delete_meta("hash0123", "filename0123", ["b"])
        assert meta_storage._data["hash0123"].meta == {"c": 3, "d": {"d.a": None}}
        assert meta_storage._data["hash0123"].filename == "filename0123"
        assert meta_storage._data["hash0123"].update_timestamp >= dt_new

        time.sleep(1e-6) # datetime in isoformat tracks microseconds
        dt_new = datetime.datetime.now(datetime.timezone.utc)
        dt_new = dt_new.replace(microsecond=int(dt_new.microsecond / 1000) * 1000)
        meta_storage.delete_meta("hash0123", "new_filename")
        assert meta_storage._data["hash0123"].meta == {}
        assert meta_storage._data["hash0123"].filename == "new_filename"
        assert meta_storage._data["hash0123"].update_timestamp >= dt_new


class TestMetaStorageStoring:
    def test_meta_storage_store_data_creates_no_meta_file_when_no_meta_data_to_be_stored(self, file_access_patcher):

        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")
        meta_storage = MetaStorage(search_path)
        assert len(meta_storage) == 0
        meta_storage._store_data()
        assert not any(f.endswith(meta_storage._meta_extension) for f in os.listdir(search_path))

        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "empty_meta_file")
        meta_files = [f for f in os.listdir(search_path) if f.endswith(meta_storage._meta_extension)]
        assert meta_files == ["random-filename.meta"]

        meta_storage = MetaStorage(search_path)
        assert len(meta_storage) == 0

        meta_storage._store_data()
        assert not any(f.endswith(meta_storage._meta_extension) for f in os.listdir(search_path))

    def test_meta_storage_store_data_writes_meta_file_if_meta_data_present(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")

        meta_storage = MetaStorage(search_path)
        assert len(meta_storage) == 3
        meta_storage._store_data()

        assert meta_storage._meta_path == os.path.join(search_path, "deadbeef.meta")
        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})

        # this assert only checks a side-effect of removing/changing the formatting
        assert file_access_patcher.overlay.get(meta_storage._meta_path).content_modified

    def test_meta_storage_store_data_does_not_modify_meta_file_on_same_data(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file")

        meta_storage = MetaStorage(search_path)
        assert len(meta_storage) == 2
        meta_storage._store_data()

        assert meta_storage._meta_path == os.path.join(search_path, "e44bf7613d7c4484aca3db39e527a7b1.meta")
        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})

        assert not file_access_patcher.overlay.get(meta_storage._meta_path).content_modified

    def test_meta_storage_store_data_writes_meta_file_when_at_least_one_entry_has_been_added(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file")

        meta_storage = MetaStorage(search_path)
        meta_storage.set_meta("hash0123", "filename0123", {"test": "test"})

        assert meta_storage._meta_path is not None
        assert meta_storage._meta_path.endswith(MetaStorage._meta_extension)
        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})

        assert file_access_patcher.overlay.get(meta_storage._meta_path).content_modified

    def test_meta_storage_store_data_writes_meta_file_when_at_least_one_entry_has_been_updated(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")

        meta_storage = MetaStorage(search_path)
        meta_storage.set_meta("hash1", "filename0123", {})
        meta_storage._store_data()

        assert meta_storage._meta_path == os.path.join(search_path, "deadbeef.meta")
        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})
        assert file_access_patcher.overlay.get(meta_storage._meta_path).content_modified

    def test_meta_storage_store_data_writes_meta_file_when_at_least_one_entry_meta_is_partially_modified(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")

        meta_storage = MetaStorage(search_path)
        meta_storage.set_meta("hash1", "2023-10-13-19-32-25-376-40x30.jpg", {"creation_timestamp": "2023-01-01T00:00:00.000000", "image_orientation": 1})
        meta_storage._store_data()

        assert meta_storage._meta_path == os.path.join(search_path, "deadbeef.meta")
        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})
        assert file_access_patcher.overlay.get(meta_storage._meta_path).content_modified

    def test_meta_storage_store_data_writes_meta_file_when_at_least_one_filename_is_partially_modified(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")

        meta_storage = MetaStorage(search_path)
        meta_storage.set_meta("hash1", "new_filename", {"creation_timestamp": "2023-01-01T00:00:00.000000", "image_orientation": 3})
        meta_storage._store_data()

        assert meta_storage._meta_path == os.path.join(search_path, "deadbeef.meta")
        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})
        assert file_access_patcher.overlay.get(meta_storage._meta_path).content_modified

    def test_meta_storage_reuses_existing_meta_file(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        search_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file")
        meta_storage = MetaStorage(search_path)

        assert meta_storage._meta_path == os.path.join(search_path, "deadbeef.meta")
        meta_storage._store_data()

        assert builtins.open.calls[-1] == ((meta_storage._meta_path, "w"), {})
