# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import os
import logging
import pytest

from photoassistant.core.filescan import FileSystemScanner

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)

class TestFileSystemScanner:
    def test_find_any_photo(self):
        search_path = pytest.utils.get_test_resources_path("photos", "sorted_10")
        photo_paths = FileSystemScanner.find_supported_files(search_path)
        assert len(photo_paths) > 0

    def test_find_all_photos_in_d1(self):
        pytest.utils.delete_all_license_files_from_resources() 
        search_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1")
        photo_paths = FileSystemScanner.find_supported_files(search_path)
        paths = [[name for name in path.replace(search_path, "").split(os.path.sep) if name != ""] for path in photo_paths]
        assert ['2023-10-13-19-32-06-345-40x30.jpg'] in paths
        assert ['2023-10-13-19-32-11-778-40x30.jpg'] in paths
        assert ['d1_3', '2023-10-13-19-32-17-102-40x30.jpg'] in paths
        assert ['d1_3', '2023-10-13-19-32-36-602-40x30.jpg'] in paths
        assert ['d1_3', '2023-10-13-19-32-40-737-40x30.jpg'] in paths
        assert ['d1_3', 'd1_3_2', '2023-10-13-19-32-25-376-40x30.jpg'] in paths
        assert ['d1_3', 'd1_3_2', '2023-10-13-19-32-31-105-40x30.jpg'] in paths
        assert len(paths) == 7

    def test_find_all_photos_in_d1_with_filter(self):
        pytest.utils.delete_all_license_files_from_resources() 
        search_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1")
        filter_function = lambda path: "d1_3" in path and not "d1_3_2" in path
        photo_paths = FileSystemScanner.find_supported_files(search_path, filter_function=filter_function)
        paths = [[name for name in path.replace(search_path, "").split(os.path.sep) if name != ""] for path in photo_paths]
        assert ['d1_3', '2023-10-13-19-32-17-102-40x30.jpg'] in paths
        assert ['d1_3', '2023-10-13-19-32-36-602-40x30.jpg'] in paths
        assert ['d1_3', '2023-10-13-19-32-40-737-40x30.jpg'] in paths
        assert len(paths) == 3
