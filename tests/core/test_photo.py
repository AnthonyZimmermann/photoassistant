# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import builtins
import datetime
import exifread
import logging
import mmap
import os
import pytest
import queue
import time

from photoassistant.core.metastorage import MetaStorage
from photoassistant.core.photo import Photo
from photoassistant.core.photo import PhotoAsyncSharedCached
from photoassistant.core.photo import PhotoHash
from photoassistant.core.photo import PhotoInterface
from photoassistant.utils.cachingutils import SizeRestrictedCache

logger = logging.getLogger(__file__)
logger.setLevel(logging.INFO)


class TestPhotoHash:
    def test_sha256(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        assert PhotoHash.sha256(photo_path) == expected_hash


class TestPhotoInterface:
    def setup_method(self):
        class PhotoInterfaceImplementation(PhotoInterface):
            @staticmethod
            def file_is_supported(path):
                return os.path.exists(path)
        self.photo_interface_cls = PhotoInterfaceImplementation

    def test_creation_for_existing_path(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        self.photo_interface_cls(photo_path)

    def test_creation_for_non_existing_path(self):
        with pytest.raises(Exception):
            self.photo_interface_cls("non-existent-path")

    def test_get_path_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            photo.path = "abc"

    def test_get_main_tag_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.main_tag
        with pytest.raises(NotImplementedError):
            photo.main_tag = ":abc"

    def test_get_tags_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.tags

    def test_get_all_tags_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.all_tags

    def test_get_and_set_creation_timestamp_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.creation_timestamp
        with pytest.raises(NotImplementedError):
            photo.creation_timestamp = "abc"

    def test_get_and_set_orientation_correction_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.orientation_correction
        with pytest.raises(NotImplementedError):
            photo.orientation_correction = "abc"

    def test_get_hash_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.hash

    def test_get_image_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.image

    def test_add_tag_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            photo.add_tag("hi")

    def test_delete_tag_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = self.photo_interface_cls(photo_path)
        with pytest.raises(NotImplementedError):
            photo.delete_tag("hi")


class PhotoImplementation(Photo):
    @staticmethod
    def file_is_supported(path):
        return os.path.exists(path)


class TestPhoto:
    def test_creation_for_existing_path_without_meta_files(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        PhotoImplementation(photo_path)

    def test_creation_for_non_existing_path_fails(self):
        with pytest.raises(Exception):
            PhotoImplementation("non-existent-path")

    def test_get_exif(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.exif is not None

    def test_get_exif_without_exif_present(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "no_exif", "2023-10-13-19-33-03-352-40x30-no-meta.jpg")
        photo = PhotoImplementation(photo_path)
        assert isinstance(photo.exif, dict)
        assert len(photo.exif) == 0

    def test_get_exif_catches_exceptions_of_exifread(self, monkeypatch):
        def mock_exifread_process_file(*args, **kwargs):
            raise Exception("Any exception that exifread.process_file raises")
        mock_exifread_process_file = pytest.utils.FunctionSniffer(mock_exifread_process_file)
        monkeypatch.setattr(exifread, "process_file", mock_exifread_process_file)

        # monkeypatch works
        with pytest.raises(Exception):
            exifread.process_file(1, 2, 3, "a", b="c")
        assert mock_exifread_process_file.calls[0] == ((1, 2, 3, "a"), {"b": "c"})

        photo_path = pytest.utils.get_test_resources_path("photos", "no_exif", "2023-10-13-19-33-03-352-40x30-no-meta.jpg")
        photo = PhotoImplementation(photo_path)
        assert isinstance(photo.get_exif(), dict)
        assert isinstance(mock_exifread_process_file.calls[1][0][0], mmap.mmap)
        assert mock_exifread_process_file.calls[1][1] == {"details": False}

        assert len(photo.get_exif()) == 0

    def test_get_creation_timestamp_from_exif(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.creation_timestamp == datetime.datetime.strptime("2023:10:13 19:32:06", "%Y:%m:%d %H:%M:%S")

    def test_get_creation_timestamp_without_exif_and_meta_returns_none(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "no_exif", "2023-10-13-19-33-03-352-40x30-no-meta.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.creation_timestamp is None

    def test_get_orientation_correction_from_exif(self):
        rotation_correction = [
            (1, [1, 0, 0, 0, 1, 0, 0, 0, 1]),
            (2, [-1, 0, 0, 0, 1, 0, 0, 0, 1]),
            (3, [-1, 0, 0, 0, -1, 0, 0, 0, 1]),
            (4, [1, 0, 0, 0, -1, 0, 0, 0, 1]),
            (5, [0, 1, 0, 1, 0, 0, 0, 0, 1]),
            (6, [0, 1, 0, -1, 0, 0, 0, 0, 1]),
            (7, [0, -1, 0, -1, 0, 0, 0, 0, 1]),
            (8, [0, -1, 0, 1, 0, 0, 0, 0, 1]),
        ]
        for exif_id, correction in rotation_correction:
            photo_path = pytest.utils.get_test_resources_path("photos", "rotated_versions", f"2023-10-13-19-32-11-778-40x30-rot-{exif_id}.jpg")
            photo = PhotoImplementation(photo_path)
            assert photo.orientation_correction is not None
            assert photo.orientation_correction == correction
            assert photo._get_exif_orientation_correction == correction

    def test_get_orientation_correction_without_exif_and_meta_returns_identity_transform(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "no_exif", "2023-10-13-19-33-03-352-40x30-no-meta.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.orientation_correction == [1, 0, 0, 0, 1, 0, 0, 0, 1]

    def test_get_hash(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.hash is not None
        assert photo.hash == "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"

    def test_get_gps_coordinates_from_exif(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-40-737-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo._get_exif_gps_coordinates() == [40.6892, -74.0445, 10.0]
        assert photo.gps_coordinates == [40.6892, -74.0445, 10.0]

    def test_get_gps_coordinates_from_meta_storage(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.gps_coordinates == [10, 20, 30]

    def test_set_gps_coordinates(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-40-737-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.gps_coordinates == [40.6892, -74.0445, 10.0]
        photo.gps_coordinates = [11, 22, 33]
        assert photo.gps_coordinates == [11, 22, 33]

    def test_set_gps_coorinates_fails_on_invalid_value(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)

        assert len([f for f in os.listdir(os.path.dirname(photo_path)) if f.endswith(MetaStorage._meta_extension)]) == 0
        assert photo.orientation_correction == [0, 1, 0, -1, 0, 0, 0, 0, 1]

        with pytest.raises(ValueError):
            photo.orientation_correction = "abc"

    def test_set_gps_coordinates_deletes_meta_storage_data_that_aligns_with_exif_data(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-40-737-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo._get_exif_gps_coordinates() == [40.6892, -74.0445, 10.0]
        assert photo.gps_coordinates == [40.6892, -74.0445, 10.0]

        photo.gps_coordinates = [11, 22, 33]
        assert photo.gps_coordinates == [11, 22, 33]

        assert MetaStorage(photo_path).get_meta(photo.hash).get("gps_coordinates") == [11, 22, 33]

        photo.gps_coordinates = [40.6892, -74.0445, 10.0]
        assert MetaStorage(photo_path).get_meta(photo.hash).get("gps_coordinates") == None

    def test_get_image_raises_not_implemented_error(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        with pytest.raises(NotImplementedError):
            _ = photo.image

    def test_creation_with_meta_storage(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        PhotoImplementation(photo_path)

    def test_creation_with_multiple_meta_storage_files(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_multiple_meta_files", "2023-10-13-19-32-17-102-40x30.jpg")
        PhotoImplementation(photo_path)

    def test_get_creation_timestamp_from_exif_while_meta_storage_file_present(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-40-737-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.creation_timestamp == datetime.datetime.strptime("2023:10:13 19:32:40", "%Y:%m:%d %H:%M:%S")

    def test_get_creation_timestamp_from_meta_storage_while_exif_present(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.creation_timestamp == datetime.datetime.strptime("2001:02:03 04:05:06", "%Y:%m:%d %H:%M:%S")

    def test_get_orientation_correction_from_exif(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo._get_exif_orientation_correction() == [0, 1, 0, -1, 0, 0, 0, 0, 1]
        assert photo.orientation_correction == [0, 1, 0, -1, 0, 0, 0, 0, 1]

    def test_set_orientation_correction_fails_on_invalid_value(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)

        assert len([f for f in os.listdir(os.path.dirname(photo_path)) if f.endswith(MetaStorage._meta_extension)]) == 0
        assert photo.orientation_correction == [0, 1, 0, -1, 0, 0, 0, 0, 1]

        with pytest.raises(ValueError):
            photo.orientation_correction = "abc"

    def test_set_orientation_correction_gets_stored_into_meta_file_and_can_be_read_back(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)

        assert len([f for f in os.listdir(os.path.dirname(photo_path)) if f.endswith(MetaStorage._meta_extension)]) == 0
        assert photo.orientation_correction == [0, 1, 0, -1, 0, 0, 0, 0, 1]

        photo.orientation_correction = [-1, 0, 0, 0, -1, 0, 0, 0, 1]

        meta_files = [f for f in os.listdir(os.path.dirname(photo_path)) if f.endswith(MetaStorage._meta_extension)]
        assert len(meta_files) == 1
        meta_file_path = os.path.join(os.path.dirname(photo_path), meta_files[0])

        assert builtins.open.calls[-1] == ((meta_file_path, "w"), {})
        assert photo.orientation_correction == [-1, 0, 0, 0, -1, 0, 0, 0, 1]
        assert file_access_patcher.overlay.get(meta_file_path).content_modified

    def test_set_orientation_correction_deletes_meta_data_that_aligns_with_exif_data(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_meta_file", "2023-10-13-19-32-40-737-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo._get_exif_orientation_correction() == [0, 1, 0, -1, 0, 0, 0, 0, 1]
        assert photo.orientation_correction == [0, 1, 0, -1, 0, 0, 0, 0, 1]

        photo.orientation_correction = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        assert photo.orientation_correction == [1, 2, 3, 4, 5, 6, 7, 8, 9]

        assert MetaStorage(photo_path).get_meta(photo.hash).get("orientation_correction") == [1, 2, 3, 4, 5, 6, 7, 8, 9]

        photo.orientation_correction = [0, 1, 0, -1, 0, 0, 0, 0, 1]
        assert MetaStorage(photo_path).get_meta(photo.hash).get("orientation_correction") == None

    def test_set_creation_timestamp_gets_stored_into_meta_file_and_can_be_read_back(self, file_access_patcher):
        builtins.open = pytest.utils.FunctionSniffer(builtins.open)
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "no_meta_file", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoImplementation(photo_path)

        assert len([f for f in os.listdir(os.path.dirname(photo_path)) if f.endswith(MetaStorage._meta_extension)]) == 0
        assert photo.meta.get("creation_timestamp") is None
        assert photo.creation_timestamp == datetime.datetime(2023, 10, 13, 19, 32, 6)

        photo.creation_timestamp = datetime.datetime(2023, 4, 5, 6, 7, 8)  # 2023:04:05-06:07:08

        meta_files = [f for f in os.listdir(os.path.dirname(photo_path)) if f.endswith(MetaStorage._meta_extension)]
        assert len(meta_files) == 1
        meta_file_path = os.path.join(os.path.dirname(photo_path), meta_files[0])

        assert builtins.open.calls[-1] == ((meta_file_path, "w"), {})
        assert file_access_patcher.overlay.get(meta_file_path).content_modified
        assert os.path.dirname(photo_path) in meta_file_path

        assert photo.meta.get("creation_timestamp") == int(datetime.datetime(2023, 4, 5, 6, 7, 8).timestamp() * 1000)
        assert photo.creation_timestamp == datetime.datetime(2023, 4, 5, 6, 7, 8)

    def test_add_main_tag(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.main_tag is None
        photo.main_tag = ":maintag"
        assert photo.main_tag == ":maintag"
        assert ":maintag" in photo.all_tags

    def test_set_main_tag_overwrites_old_main_tags(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert photo.main_tag is None
        photo.main_tag = ":maintag"
        assert photo.main_tag == ":maintag"
        assert ":maintag" in photo.all_tags

        photo.main_tag = ":new:main-tag"
        assert photo.main_tag == ":new:main-tag"

        assert ":new:main-tag" in photo.all_tags

    def test_set_main_tag_fails_if_main_tag_does_not_start_with_colon(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        with pytest.raises(Photo.TagFormatException):
            photo.main_tag = "ill-formatted-main-tag"

    def test_delete_main_tag_by_setting_to_None(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-31-105-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert len(photo.tags) == 3
        assert len(photo.all_tags) == 4
        photo.main_tag = None
        assert len(photo.tags) == 3
        assert len(photo.all_tags) == 3
        assert photo.main_tag is None
        assert all(t in photo.tags and t in photo.all_tags for t in ("photo-tag-1", "photo-tag-2", "photo-tag-3"))

    def test_add_tag(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert len(photo.tags) == 0
        assert len(photo.all_tags) == 0
        photo.add_tag("testtag")
        assert len(photo.tags) == 1
        assert len(photo.all_tags) == 1
        assert "testtag" in photo.tags
        assert "testtag" in photo.all_tags

    def test_add_tags(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-25-376-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert len(photo.tags) == 0
        assert len(photo.all_tags) == 0
        photo.add_tag("testtag1")
        photo.add_tag("testtag2")
        photo.add_tag("testtag3")
        photo.add_tag("testtag4")
        assert len(photo.tags) == 4
        assert len(photo.all_tags) == 4
        assert all(t in photo.tags and t in photo.all_tags for t in ("testtag1", "testtag2", "testtag3", "testtag4"))

    def test_get_tags_loads_from_meta_file(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-31-105-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert len(photo.tags) == 3
        assert len(photo.all_tags) == 4
        assert photo.main_tag == ":photo-main-tag"
        assert "photo-tag-1" in photo.tags
        assert "photo-tag-2" in photo.tags
        assert "photo-tag-3" in photo.tags

    def test_delete_tag(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "meta_storage_test", "no_broken_meta_files", "with_autoformatted_meta_file", "2023-10-13-19-32-31-105-40x30.jpg")
        photo = PhotoImplementation(photo_path)
        assert len(photo.tags) == 3
        assert len(photo.all_tags) == 4
        photo.delete_tag("photo-tag-2")
        assert len(photo.tags) == 2
        assert len(photo.all_tags) == 3
        assert all(t in photo.tags and t in photo.all_tags for t in ("photo-tag-1", "photo-tag-3"))
        assert photo.main_tag == ":photo-main-tag"


class PhotoAsyncSharedCachedImplementation(PhotoAsyncSharedCached):
    @staticmethod
    def file_is_supported(path):
        return os.path.exists(path)


class TestPhotoAsyncSharedCached:
    def setup_method(self):
        PhotoAsyncSharedCachedImplementation.ASYNC_SHARED_CACHE_EXIF._cache = SizeRestrictedCache(slots=1)
        PhotoAsyncSharedCachedImplementation.ASYNC_SHARED_CACHE_EXIF.set_slots(100000)
        PhotoAsyncSharedCachedImplementation.ASYNC_SHARED_CACHE_HASH._cache = SizeRestrictedCache(slots=1)
        PhotoAsyncSharedCachedImplementation.ASYNC_SHARED_CACHE_HASH.set_slots(100000)

    # test get_exif function
    def test_get_exif_as_property(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        assert photo.exif is not None

    def test_get_exif_without_cache(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 0
        assert photo.get_exif(query_ignore_cache=True) is not None
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 0

    def test_get_exif_with_cache(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 0
        assert photo.get_exif() is not None
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 1

    def test_get_exif_with_cache_with_result_placeholder(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 0
        assert photo.get_exif(query_result_placeholder="<placeholder>") == "<placeholder>"
        photo.get_exif()  # force computing and caching the result
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 1
        assert photo.get_exif(query_result_placeholder="<placeholder>") != "<placeholder>"
        assert isinstance(photo.get_exif(query_result_placeholder="<placeholder>"), dict)
        assert len(photo.get_exif(query_result_placeholder="<placeholder>")) > 0

    def test_get_exif_with_cache_with_callback(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        async_results = queue.Queue()
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 0
        assert isinstance(photo.get_exif(query_callback=lambda result: async_results.put(result)), dict)
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 1
        assert isinstance(async_results.get(timeout=0.1), dict)
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 1

    def test_get_exif_with_cache_with_result_placeholder_with_callback(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        async_results = queue.Queue()
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 0
        assert photo.get_exif(query_callback=lambda result: async_results.put(result), query_result_placeholder="<placeholder>") == "<placeholder>"
        assert isinstance(async_results.get(timeout=0.1), dict)
        assert photo.get_exif(query_result_placeholder="<placeholder>") != "<placeholder>"
        assert isinstance(photo.get_exif(query_result_placeholder="<placeholder>"), dict)
        assert len(photo.ASYNC_SHARED_CACHE_EXIF._cache) == 1

    # test get_hash function
    def test_get_hash_as_property(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        assert photo.hash == expected_hash

    def test_get_hash_without_cache(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 0
        assert photo.get_hash(query_ignore_cache=True) == expected_hash
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 0

    def test_get_hash_with_cache(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 0
        assert photo.get_hash() == expected_hash
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 1

    def test_get_hash_with_cache_with_result_placeholder(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 0
        assert photo.get_hash(query_result_placeholder="<placeholder>") == "<placeholder>"
        photo.get_hash()  # force computing and caching the result
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 1
        assert photo.get_hash(query_result_placeholder="<placeholder>") == expected_hash

    def test_get_hash_with_cache_with_callback(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        async_results = queue.Queue()
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 0
        assert photo.get_hash(query_callback=lambda result: async_results.put(result)) == expected_hash
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 1
        assert async_results.get(timeout=0.1) == expected_hash
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 1

    def test_get_hash_with_cache_with_result_placeholder_with_callback(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        async_results = queue.Queue()
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 0
        assert photo.get_hash(query_callback=lambda result: async_results.put(result), query_result_placeholder="<placeholder>") == "<placeholder>"
        assert async_results.get(timeout=0.1) == expected_hash
        assert photo.get_hash(query_result_placeholder="<placeholder>") == expected_hash
        assert len(photo.ASYNC_SHARED_CACHE_HASH._cache) == 1

    def test_get_hash_speedup(self):
        photo_path = pytest.utils.get_test_resources_path("photos", "sorted_10", "d1", "2023-10-13-19-32-06-345-40x30.jpg")
        photo = PhotoAsyncSharedCachedImplementation(photo_path)
        expected_hash = "2e2b45fc459e056995e2dca517be7336e2b21f49bf2136b66836ebc1c73612fe"
        expected_minimum_speedup = 4

        t0 = time.time()
        assert photo.hash == expected_hash
        t1 = time.time()
        assert photo.hash == expected_hash
        t2 = time.time()
        hash_calculation_time = (t1 - t0)
        hash_cache_query_time = (t2 - t1)
        assert hash_calculation_time > (expected_minimum_speedup * hash_cache_query_time)
