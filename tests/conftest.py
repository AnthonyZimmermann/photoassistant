# SPDX-FileCopyrightText: 2023 Anthony Zimmermann
#
# SPDX-License-Identifier: GPL-2.0-only

import builtins
import os
import pytest
import shutil
import tempfile
import typing

class FileSystemAccessPatcher:
    builtins_open: typing.Callable = builtins.open
    os_listdir: typing.Callable = os.listdir
    os_mkdir: typing.Callable = os.mkdir
    os_remove: typing.Callable = os.remove
    os_rmdir: typing.Callable = os.rmdir
    os_scandir: typing.Callable = os.scandir
    os_path_exists: typing.Callable = os.path.exists
    os_path_isdir: typing.Callable = os.path.isdir
    os_path_isfile: typing.Callable = os.path.isfile
    shutil_copy: typing.Callable = shutil.copy
    shutil_move: typing.Callable = shutil.move
    shutil_rmtree: typing.Callable = shutil.rmtree

    @staticmethod
    def split_path(path):
        path_abs = os.path.abspath(os.path.normpath(path))

        path_splitted = [path_abs]
        while True:
            path_part, path_item = os.path.split(path_splitted[0])
            if path_item == "":
                break
            path_splitted = [path_part, path_item] + path_splitted[1:]
        return path_splitted

    @staticmethod
    def readbytes(path):
        with FileSystemAccessPatcher.builtins_open(path, "rb") as file_:
            return file_.read()

    @staticmethod
    def copyfile(source, destination):
        with FileSystemAccessPatcher.builtins_open(source, "rb") as source_file:
            with FileSystemAccessPatcher.builtins_open(destination, "wb") as destination_file:
                destination_file.write(source_file.read())

    class OverlayNode:
        def __init__(self, overlay_root, path):
            self.overlay_root = overlay_root
            self.path = path
            self.overlay_nodes = dict()
            self.meta = dict()

            os_path = self.os_path
            overlay_path = self.overlay_os_path
            if FileSystemAccessPatcher.os_path_exists(os_path):
                if overlay_path == self.overlay_root:
                    return
                if FileSystemAccessPatcher.os_path_isdir(os_path):
                    FileSystemAccessPatcher.os_mkdir(overlay_path)
                elif FileSystemAccessPatcher.os_path_isfile(os_path):
                    FileSystemAccessPatcher.copyfile(os_path, overlay_path)
                else:
                    raise NotImplementedError(f"Path '{os_path}' exists but is not recognized as file or as directory")

        @property
        def os_path(self):
            return os.path.join(*self.path)

        @property
        def overlay_os_path(self):
            return os.path.join(self.overlay_root, *self.path[1:])

        @property
        def original_content(self):
            os_path = self.os_path
            if not FileSystemAccessPatcher.os_path_exists(os_path):
                return None
            assert FileSystemAccessPatcher.os_path_isfile(os_path)
            return FileSystemAccessPatcher.readbytes(self.os_path)

        @property
        def content(self):
            overlay_os_path = self.overlay_os_path
            assert FileSystemAccessPatcher.os_path_isfile(overlay_os_path)
            return FileSystemAccessPatcher.readbytes(overlay_os_path)

        @property
        def content_modified(self):
            return self.content != self.original_content

        def _track_node(self, node):
            if node not in self.overlay_nodes:
                overlay_child_node_path = self.path + [node]
                overlay_child_node = FileSystemAccessPatcher.OverlayNode(
                    self.overlay_root,
                    overlay_child_node_path,
                )
                self.overlay_nodes[node] = overlay_child_node

        def get(self, nodes=[]):
            if len(nodes) == 0:
                os_path = self.os_path
                if FileSystemAccessPatcher.os_path_isdir(os_path):
                    for node in FileSystemAccessPatcher.os_listdir(os_path):
                        self._track_node(node)
                        self.overlay_nodes[node].get()
                return self
            else:
                node = nodes.pop(0)
                self._track_node(node)
                return self.overlay_nodes[node].get(nodes)

    class Overlay:
        def __init__(self):
            self.overlay_root = tempfile.mkdtemp()
            self.overlay_nodes = dict()

        def get(self, path):
            path = os.path.abspath(path)
            nodes = FileSystemAccessPatcher.split_path(path)
            root = nodes.pop(0)
            if root not in self.overlay_nodes:
                overlay_node = FileSystemAccessPatcher.OverlayNode(self.overlay_root, [root])
                self.overlay_nodes[root] = overlay_node
            return self.overlay_nodes[root].get(nodes)

    def __init__(self):
        self.overlay = FileSystemAccessPatcher.Overlay()
        self.overlay_nodes_opened = dict()

    def cleanup(self):
        FileSystemAccessPatcher.shutil_rmtree(self.overlay.overlay_root)

    def patch_generic_single_path_func(self, path_func, pass_through_condition=lambda path, args, kwargs: False):
        def _generic_single_path_func_patched(path, *args, **kwargs):
            if pass_through_condition(path, args, kwargs):
                return path_func(path, *args, **kwargs)

            if path.startswith(self.overlay.overlay_root):
                path = path[len(self.overlay.overlay_root):]
                assert not path.startswith(self.overlay.overlay_root)
            overlay_node = self.overlay.get(path)
            return path_func(overlay_node.overlay_os_path, *args, **kwargs)
        return _generic_single_path_func_patched

    def patch_generic_two_paths_func(self, two_paths_func):
        def _generic_two_paths_func_patched(path_1, path_2, *args, **kwargs):
            overlay_node_1 = self.overlay.get(path_1)
            overlay_node_2 = self.overlay.get(path_2)
            return two_paths_func(
                overlay_node_1.overlay_os_path, 
                overlay_node_2.overlay_os_path,
                *args,
                **kwargs,
            )
        return _generic_two_paths_func_patched


class Utils:
    TEST_RESOURCES_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), "resources")
    @staticmethod
    def get_test_resources_path(*dirs):
        return os.path.join(Utils.TEST_RESOURCES_PATH, *dirs)

    class FunctionSniffer:
        def __init__(self, func):
            assert not isinstance(func, Utils.FunctionSniffer)
            self.calls = []
            self.func = func

        def __call__(self, *args, **kwargs):
            self.calls.append((args, kwargs))
            return self.func(*args, **kwargs)

    @staticmethod
    def delete_all_license_files_from_resources():
        for root, _, files in os.walk(Utils.get_test_resources_path()):
            root_path = os.path.abspath(root)
            for file in files:
                abs_path = os.path.join(root_path, file)
                _, ext = os.path.splitext(abs_path)
                if ext == ".license":
                    os.remove(abs_path)


def pytest_configure(config):
    assert not hasattr(pytest, "utils")
    setattr(pytest, "utils", Utils)

@pytest.fixture(autouse=True)
def file_access_patcher(monkeypatch):
    patcher = FileSystemAccessPatcher()
    path_is_descriptor=lambda path, args, kwargs: isinstance(path, int)
    dir_fd_in_kwargs=lambda path, args, kwargs: "dir_fd" in kwargs

    monkeypatch.setattr(builtins, "open", patcher.patch_generic_single_path_func(builtins.open, pass_through_condition=path_is_descriptor))

    monkeypatch.setattr(os, "listdir", patcher.patch_generic_single_path_func(os.listdir))
    monkeypatch.setattr(os, "makedirs", patcher.patch_generic_single_path_func(os.makedirs))
    monkeypatch.setattr(os, "mkdir", patcher.patch_generic_single_path_func(os.mkdir, pass_through_condition=dir_fd_in_kwargs))
    monkeypatch.setattr(os, "remove", patcher.patch_generic_single_path_func(os.remove, pass_through_condition=dir_fd_in_kwargs))
    monkeypatch.setattr(os, "rmdir", patcher.patch_generic_single_path_func(os.rmdir, pass_through_condition=dir_fd_in_kwargs))
    monkeypatch.setattr(os, "scandir", patcher.patch_generic_single_path_func(os.scandir, pass_through_condition=path_is_descriptor)) # used by os.walk
    monkeypatch.setattr(os.path, "exists", patcher.patch_generic_single_path_func(os.path.exists, pass_through_condition=path_is_descriptor))
    monkeypatch.setattr(os.path, "isdir", patcher.patch_generic_single_path_func(os.path.isdir))
    monkeypatch.setattr(os.path, "isfile", patcher.patch_generic_single_path_func(os.path.isfile))

    monkeypatch.setattr(shutil, "copy", patcher.patch_generic_two_paths_func(shutil.copy))
    monkeypatch.setattr(shutil, "move", patcher.patch_generic_two_paths_func(shutil.move))
    monkeypatch.setattr(shutil, "rmtree", patcher.patch_generic_single_path_func(shutil.rmtree, pass_through_condition=path_is_descriptor))

    yield patcher

    patcher.cleanup()


@pytest.fixture(autouse=True)
def meta_storage_singleton_patcher():
    from photoassistant.core.metastorage import MetaStorage
    MetaStorage._instances = dict()

@pytest.fixture(autouse=True)
def size_restricted_cache_patcher(monkeypatch):
    from photoassistant.utils.cachingutils import SizeRestrictedCache

    def wait_for_delete_thread(self):
        cache_delete_thread = self._delete_old_cache_objects_worker_thread
        if cache_delete_thread is not None:
            cache_delete_thread.join()
         
    monkeypatch.setattr(SizeRestrictedCache, "DELETE_THREAD_SLEEP_1", (SizeRestrictedCache.DELETE_THREAD_SLEEP_1 * 0.001))
    monkeypatch.setattr(SizeRestrictedCache, "DELETE_THREAD_SLEEP_2", (SizeRestrictedCache.DELETE_THREAD_SLEEP_2 * 0.001))
    monkeypatch.setattr(SizeRestrictedCache, "wait_for_delete_thread", wait_for_delete_thread, raising=False)  # raising=False -> attribute 'wait_for_delete_thread' does not yet exist
