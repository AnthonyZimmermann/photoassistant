REM SPDX-FileCopyrightText: 2023 Anthony Zimmermann
REM
REM SPDX-License-Identifier: GPL-2.0-only
REM
REM This script is as a helper script for windows users to automate the installation of
REM PhotoAssistant and a desktop shortcut for PhotoAssistant.

@echo off
setlocal

REM Find desktop path with powershell
for /f "usebackq tokens=*" %%f in (`powershell -NoProfile -Command "Write-Host([Environment]::GetFolderPath('Desktop'))"`) do (
    set "desktop_path=%%f"
)
set "shortcut_path=%desktop_path%\PhotoAssistant.lnk"

REM Setup locations for the copy of this batch script; This operation might fail if photoassistant is not installed and needs to be executed again later.
for /f "usebackq tokens=*" %%p in (`python3 -c "import importlib.resources; print(importlib.resources.files('photoassistant').joinpath('photoassistant.bat'))"`) do (
    set "batch_script_path=%%p"
)
REM Start PhotoAssistant if shortcut already exists and script was executed from the desktop
if exist "%batch_script_path%" (
    fc "%~f0" "%batch_script_path%" > nul
    if not errorlevel 1 (
        REM File at batch_script_path is identical to this file, execute PhotoAssistant
        goto startphotoassistant
    )
)

REM Install photoassistant such that the package directory and resources are created
python3 -m pip install --upgrade pip photoassistant

REM Find the location of the ico file
for /f "usebackq tokens=*" %%p in (`python3 -c "import importlib.resources; print(importlib.resources.files('photoassistant').joinpath('photoassistant-icon.ico'))"`) do (
    set "icon_path_ico=%%p"
)
REM Setup locations for the copy of this batch script; The operation is executed again the first execution might failed
for /f "usebackq tokens=*" %%p in (`python3 -c "import importlib.resources; print(importlib.resources.files('photoassistant').joinpath('photoassistant.bat'))"`) do (
    set "batch_script_path=%%p"
)
REM Setup locations for launcher.vbs to launch PhotoAssistant without a terminal being opened
for /f "usebackq tokens=*" %%p in (`python3 -c "import importlib.resources; print(importlib.resources.files('photoassistant').joinpath('launcher.vbs'))"`) do (
    set "launcher_path=%%p"
)

REM Copy this script into python sources directory (installation of photoassistant)
copy "%~f0" "%batch_script_path%"
REM Create a launcher.vbs which executes the batch script without opening a terminal
echo Set WshShell = CreateObject("WScript.Shell") > "%launcher_path%"
echo WshShell.Run "cmd /c %batch_script_path%", 0 >>"%launcher_path%"
echo Set WshShell = Nothing >> "%launcher_path%"

set powershell_command_create_shortcut=^
$shortcut = (New-Object -ComObject WScript.Shell).createShortcut('%shortcut_path%');^
$shortcut.TargetPath = '%launcher_path%';^
$shortcut.IconLocation = '%icon_path_ico%';^
$shortcut.Save();

powershell -Command %powershell_command_create_shortcut%
exit /b

:startphotoassistant
REM Update photoassistant in the background
start /B cmd /C "python3 -m pip install --upgrade pip photoassistant"

REM Start PhotoAssistant
python3 -m photoassistant.gui.qt.entry
exit /b
